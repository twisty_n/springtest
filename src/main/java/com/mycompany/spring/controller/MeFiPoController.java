/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.spring.controller;

import com.mycompany.spring.models.MePoThread;
import com.mycompany.spring.models.ResearchGroup;
import com.mycompany.spring.models.User;
import com.mycompany.spring.service.ThreadDocService;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 * Handles requests related to meetings,document upload and Posts
 * @author tristan
 */
@Controller
public class MeFiPoController {

    ThreadDocService threadocService;

    @Autowired
    public void setThreadocService(ThreadDocService threadocService) {
        this.threadocService = threadocService;
    }

    @RequestMapping(value = "coord/doCreateMeeting")
    public ModelAndView doCreateMeeting (
            Model model,
            @Valid MePoThread meeting,
            BindingResult result,
            HttpSession session,
            final RedirectAttributes redirAt) {

        ModelAndView mav;

        ResearchGroup g = ( (ResearchGroup )session.getAttribute("group") );

        //First validate our date to make sure that it is at leat a day in advance
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd:HH:mm:ss");
        Date date;
        Calendar meetingDay = new GregorianCalendar();
        Calendar rightNowPlusADay = new GregorianCalendar();
        rightNowPlusADay.add(Calendar.DATE, 1);
        //Format our date so the parser doesn't bitch
        meeting.setDateAndTime(
                meeting.getDateAndTime()
                        .replace("T", ":")
                        .concat(":00")  );

        try {
            date = ( Date ) df.parse( meeting.getDateAndTime() );
            meetingDay.setTime(date);
        } catch (ParseException ex) {
            mav = new ModelAndView();
            mav.setViewName("redirect:/coord/editGroup?group="+g.getGroupId() );
            redirAt.addFlashAttribute("dateError", "An unknown error has occured, meeting not recorded");
            Logger.getLogger(MeFiPoController.class.getName()).log(Level.SEVERE, null, ex);
            return mav;
        }

        if ( meetingDay.before( rightNowPlusADay ) ) {
            //The meeting is less than 24 hours before it should be
            mav = new ModelAndView();
            mav.setViewName("redirect:/coord/editGroup?group="+g.getGroupId() );
            redirAt.addFlashAttribute("dateError", "Please make the meeting at least a day in the future");
            return mav;
        }

        //Everything is valid. Add the meeting to the Database
        User thisUser = ( ( User ) session.getAttribute( "thisUser" ) );
        meeting.setGroupId( g.getGroupId() );
        meeting.setUserId( thisUser.getUserId() );
        meeting.setPoster( thisUser );
        meeting.setThreadCreator( thisUser );

        this.threadocService.createMeeting(meeting,
                (List<User>) (session.getAttribute("inGroup") ) );
        //yay success
        mav = new ModelAndView();
        mav.setViewName("redirect:/coord/editGroup?group="+g.getGroupId() );
            redirAt.addFlashAttribute("successYay", "Meeting has been set for time: "+meetingDay.getTime().toString() );

        return mav;

    }


}
