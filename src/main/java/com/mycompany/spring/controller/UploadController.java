/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.spring.controller;

import com.mycompany.spring.models.DocumentUpLoadContainer;
import com.mycompany.spring.models.EnhancedUser;
import com.mycompany.spring.models.ProfilePhotoUploadContainer;
import com.mycompany.spring.models.ResearchGroup;
import com.mycompany.spring.models.User;
import com.mycompany.spring.service.ResearchGroupService;
import com.mycompany.spring.service.ThreadDocService;
import com.mycompany.spring.service.UserService;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 * Handles all of the file upload requests including those for user profile
 * images and document uploads
 *
 * @author tristan
 */
@Controller
public class UploadController implements HandlerExceptionResolver {

    private UserService userService;
    private ResearchGroupService groupService;
    private ThreadDocService threadService;

    @Autowired
    public void setThreadDoceService ( ThreadDocService tds ) {
        this.threadService = tds;
    }

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @Autowired
    public void setGroupService(ResearchGroupService groupService) {
        this.groupService = groupService;
    }

    @Autowired
    ServletContext context;

    //upload/doUploadPhoto
    @RequestMapping(value = "/upload/doUploadPhoto")
    public String doUploadPicture(
            @ModelAttribute(value = "photo") ProfilePhotoUploadContainer p,
            HttpSession session) {

        if (  ! p.getFile().isEmpty() ) {
            //Process the form data
            try {

                BufferedImage src = ImageIO.read( new ByteArrayInputStream( p.getFile().getBytes() ) );
                File dest = new File(
                        context.getRealPath( "/resources/userImage" )
                        + ( ( User ) session.getAttribute("thisUser") ).getUserId() );
                ImageIO.write(src, "png", dest);

                this.userService.updatePhotoUrl( (
                        ( User ) session.getAttribute("thisUser") ).getUserId(),
                        "userImage" + ( ( User ) session.getAttribute("thisUser") ).getUserId() );

                return "redirect:/landing";

            } catch (IOException e) {
                return e.toString();
            }
        } else {
            return "redirect:/common/edit-profile";
        }

    }

    @RequestMapping(value = "upload/doUploadDoc")
    public ModelAndView doUploadFile(
            @ModelAttribute(value = "documentContainer") DocumentUpLoadContainer duc,
            BindingResult result,
            HttpSession session,
            RedirectAttributes redir) {

        Integer groupId = ( ( ResearchGroup ) session.getAttribute("group") ).getGroupId();
        ModelAndView view = new ModelAndView("redirect:/common/viewg?g="+groupId);

        if ( result.hasErrors() ) {
            //Errors redirect with error script
            view.setViewName("redirect:/common/upload-doc");
            redir.addFlashAttribute("error", "There was an error in your input, please try again");
            return view;
        }

        User thisUser = ( (User) session.getAttribute("thisUser") );
        List<EnhancedUser> userList = ( (List<EnhancedUser>)(session.getAttribute("userList") ) );

        //Exctracts our email list because Java doesnt like the casing otherwise
        List<User> emailList = new ArrayList<User>();
        for ( EnhancedUser user : userList ) {
            emailList.add( user.getThis() );
        }

        //Make sure our object contains all the right data
        duc.setGroupId( groupId );
        duc.setPoster( thisUser );
        duc.setThreadCreator( thisUser );
        duc.setUserId( thisUser.getUserId() );
        duc.setFilename( duc.getFile().getOriginalFilename() );

        Integer uniqueId = this.threadService.createAndRetrieveFileRecord(
                duc,
                emailList );

        File dest = new File(
                        context.getRealPath( "/resources/" )
                        +"/"+ uniqueId +"-"+ thisUser.getUserName() +"-"+ duc.getFile().getOriginalFilename() );



        FileOutputStream outputStream;
        try {

            outputStream = new FileOutputStream( dest );
            outputStream.write( duc.getFile().getBytes() );
            outputStream.close();

        } catch ( Exception e ) {
            Logger.getLogger(MeFiPoController.class.getName()).log(
                    Level.SEVERE,
                    "Upload Failed.", e );
            view.setViewName( "redirect:/common/upload-doc" );
            redir.addFlashAttribute( "error", "An unknown error has occured" );
            return view;
        }

        return view;
    }

    @Override
    public ModelAndView resolveException(
            HttpServletRequest request,
            HttpServletResponse response,
            Object o,
            Exception ex) {

        //Not sure what this is for
        return null;

    }

}
