/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.spring.controller;

import com.mycompany.spring.models.DocumentUpLoadContainer;
import com.mycompany.spring.models.EnhancedUser;
import com.mycompany.spring.models.FiPoThread;
import com.mycompany.spring.models.Post;
import com.mycompany.spring.models.ProfileContainer;
import com.mycompany.spring.models.ProfilePhotoUploadContainer;
import com.mycompany.spring.models.ResearchGroup;
import com.mycompany.spring.models.User;
import com.mycompany.spring.service.ResearchGroupService;
import com.mycompany.spring.service.ThreadDocService;
import com.mycompany.spring.service.UserService;
import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStream;
import java.net.URL;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

/**
 *  Handles all of the requests for the common pages
 * @author Tristan
 */
@Controller
public class CommonController {

    private UserService userService;
    private ResearchGroupService groupService;
    private ThreadDocService tdService;

    @Autowired
    ServletContext context;

    @Autowired
    public void setThreadDocService( ThreadDocService tds ) {
        this.tdService = tds;
    }

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @Autowired
    public void setGroupService(ResearchGroupService groupService) {
        this.groupService = groupService;
    }

    @RequestMapping(value = "/common/edit-profile")
    public ModelAndView goToEditProfile () {

        ModelAndView view = new ModelAndView(
                "/common/EditProfile" ).addObject( "profile",
                        new ProfileContainer() ).addObject("photo",
                                new ProfilePhotoUploadContainer());

        return view;
    }

     @RequestMapping(value = "/common/doEditProfile")
    public String goToEditProfile (
            Model model,
            @Valid ProfileContainer newProfileContainer,
            BindingResult result,
            HttpSession session) {

        if ( result.hasErrors() ) {
            return "redirect:/common/edit-profile";
         } else {
            //Update the information
            this.userService.updateUserProfile(
                    newProfileContainer,
                    ( ( User ) session.getAttribute( "thisUser" ) ).getUserId() );
           return "redirect:/common/edit-profile";
         }
    }

    @RequestMapping(value = "/common/viewg")
    public ModelAndView viewGroup(
            @RequestParam(value = "g") Integer groupId,
            HttpSession session) {

        ModelAndView view = new ModelAndView("common/bad-access");
        boolean validAccess = false;
        List<EnhancedUser> userList = this.userService.getAllEnhancedUsersInGroup( groupId  );

        for ( EnhancedUser user : userList ) {
            //Ensures only valid users can see the group
            if ( user.getUserId() == ( (User) session.getAttribute("thisUser") ).getUserId() ) {
                validAccess = true;
                break;
            }
        }

        if ( validAccess ) {
            //Display the group information
            view.addObject("threads", tdService.viewDiscussionThreadList( groupId ) );
            session.setAttribute("files", this.tdService.viewFileThreadList (groupId ) );
            view.addObject("meetings" , this.tdService.viewMeetingList( groupId ) );
            view.setViewName( "/common/viewg" );
            session.setAttribute("userList", userList);
            session.setAttribute("group",
                    this.groupService.retreiveResearchGroup( groupId ) ) ;

            return view;
        }

        //Else display a fun error page
        return view;
    }

    @RequestMapping(value = "common/viewp")
    public ModelAndView viewUser(
            HttpSession session,
            @RequestParam(value = "id") Integer userId ) {

        boolean validAccess = false;

        EnhancedUser userToView = null;

        for ( EnhancedUser user : ( ( List<EnhancedUser> ) session.getAttribute( "userList" ) ) ) {
            if ( user.getUserId() == userId ) {
                userToView = user;
                validAccess = true;
                break;
            }
        }

        if ( validAccess ) {
            //Get some additional information
            ModelAndView mav = new ModelAndView("/common/viewu", "userToView", userToView);
            mav.addObject("g", (ResearchGroup)session.getAttribute("group") )   ;
            return mav;
        } else {
            return new ModelAndView("/common/bad-access");
        }

    }

    @RequestMapping(value = "common/view-thread")
    public ModelAndView viewThread(
        HttpSession session,
        @RequestParam("thread") Integer threadId) {

        Post det = this.tdService.retrieveThreadDetails( threadId );

        User thisUser = ( (User) session.getAttribute("thisUser") );

        if ( det.getGroupId() != ( ( ResearchGroup ) session.getAttribute("group") ).getGroupId() ) {
            return new ModelAndView( "bad-access" );
        }

        ModelAndView view = new ModelAndView( "common/view-thread" );
        view.addObject("threadDetails", this.tdService.retrieveThreadDetails( threadId ) );
        view.addObject("posts",  this.tdService.viewPosts( threadId, thisUser ) );
        view.addObject("post", new Post() );
        session.setAttribute("threadId", threadId);
        view.addObject("g", (ResearchGroup)session.getAttribute("group") );

        return view;
    }

    @RequestMapping(value = "common/doPost")
    public ModelAndView doPost( HttpSession session,
            Model model,
            @Valid Post userPost,
            BindingResult result ) {

         String returnUrl = "redirect:/common/view-thread?thread="
                + ( (Integer) session.getAttribute( "threadId" ) );
        ModelAndView view = new ModelAndView(returnUrl);

        if ( result.hasErrors() ) {
           return view;
        }

        userPost.setPoster( ( ( User ) session.getAttribute("thisUser") ) );
        userPost.setThreadId( ( ( Integer) session.getAttribute( "threadId" ) ));
        view.addObject("g", (ResearchGroup)session.getAttribute("group") );

        this.tdService.createPost(userPost);

        return view;
    }

    @RequestMapping(value = "common/create-thread")
    public ModelAndView createThread(
        HttpSession session) {

        ModelAndView view = new ModelAndView("/common/create-thread");
        view.addObject("threadPost", new Post() );
        view.addObject("g", (ResearchGroup)session.getAttribute("group") );

        return view;
    }

    @RequestMapping(value = "common/createNewThread")
    public ModelAndView doCreateThread(
            Model model,
            @Valid Post newThreadPost,
            BindingResult result,
            HttpSession session) {

        Integer groupId = ( ( ResearchGroup ) session.getAttribute("group") ).getGroupId();
        User thisUser = ( (User) session.getAttribute("thisUser") );

        newThreadPost.setGroupId( groupId );
        newThreadPost.setUserId( thisUser.getUserId() );
        newThreadPost.setPoster( thisUser );

        this.tdService.createThread( newThreadPost );

        return new ModelAndView("redirect:/common/viewg?g="+groupId);
    }

    @RequestMapping(value = "common/upload-doc")
    public ModelAndView uploadDoc(
        HttpSession session) {

        ModelAndView view = new ModelAndView( "common/upload-doc" );
        view.addObject("documentContainer", new DocumentUpLoadContainer() );
        view.addObject("g", (ResearchGroup)session.getAttribute("group") )   ;

        return view;

    }

    @RequestMapping(value = "common/download")
    public ModelAndView doDownload(HttpSession session,
            @RequestParam(value = "ref") Integer index,
            HttpServletResponse response) {

        Integer groupId = ( ( ResearchGroup ) session.getAttribute("group") ).getGroupId();
        ModelAndView view = new ModelAndView("redirect:/common/viewg?g="+groupId);
        List<FiPoThread> files = (List<FiPoThread>) session.getAttribute("files");

        final int BUFFER_SIZE = 4096;

        try {

            //Set up ou resources
            String resource = context.getRealPath( "/resources/");
            resource = resource+ "/" + files.get(index).getFilename();

            File file = new File( resource );
            FileInputStream inputStream = new FileInputStream( file );

            //Get content tyoe
            String mimeType = context.getMimeType(resource);
            if ( mimeType == null ) {
                mimeType = "application/octet-stream";
            }
            //set the content types
            response.setContentType( mimeType );
            response.setContentLength( ( int ) file.length() );

            String headerKey = "Content-Disposition";
            String headerValue = String.format("attachment; filename=\"%s\"",
                file.getName() );

            //Set the response header
            response.setHeader( headerKey, headerValue );

            OutputStream out = response.getOutputStream();

            byte[] buffer = new byte[BUFFER_SIZE];
            int bytesRead = -1;

            // write bytes read from the input stream into the output stream
            while ( ( bytesRead = inputStream.read ( buffer ) ) != -1) {
                out.write( buffer, 0, bytesRead );
            }

            inputStream.close();
            out.close();

        } catch ( Exception e ) {
            Logger.getLogger(MeFiPoController.class.getName()).log(
                    Level.SEVERE,
                    "Download Failed.", e );
            view.addObject("docerror", "Error downloading document");
            return view;
        }

        return view;

    }

}
