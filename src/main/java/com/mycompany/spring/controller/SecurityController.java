/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.spring.controller;

import com.mycompany.spring.models.User;
import com.mycompany.spring.service.ResearchGroupService;
import com.mycompany.spring.service.UserService;
import java.security.Principal;
import java.util.Collection;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author tristan
 */
@Controller
public class SecurityController {

    private UserService userService;
    private ResearchGroupService groupService;

    @Autowired
    public void setGroupService ( ResearchGroupService rgs ) {
        this.groupService = rgs;
    }

    @Autowired
    public void setUserService( UserService userService ) {
        this.userService = userService;
    }

    @RequestMapping(value = "/user-login")
    public ModelAndView goToLoginArea() {

        return new ModelAndView( "login-form" );
    }

    @RequestMapping(value= "/error-login")
    public ModelAndView goToLoginError () {
        return this.goToLoginArea().addObject("error", true);
    }

    //Create Success handler
    @RequestMapping(value = "/index")
    public ModelAndView goToHome() {
        return  new ModelAndView("login-form");
    }

      //Create Success handler
    @RequestMapping(value = "/")
    public ModelAndView goToHomeStandard() {
        return  new ModelAndView("login-form");
    }

    @RequestMapping(value = "/landing")
    public ModelAndView goToInitPage (
            Principal principal,
            HttpServletRequest r,
            HttpSession session,
        Authentication authentication) {

        if ( authentication == null ) {
            //Attempts to stop some bizarre error
            return this.goToLoginError();
        }

        //TODO remove session attributes that are not needed

        Collection<? extends GrantedAuthority> authorities = authentication.getAuthorities();

        boolean authorized = authorities.contains(new SimpleGrantedAuthority( "ROLE_ADMIN" ) );

        if ( authorized ) {
             return new ModelAndView("admin/admin-landing");
        } else {
            session.setAttribute("thisUser", this.userService.retrieveUser( principal.getName()  ) );
            return new ModelAndView("common/landing")
                    .addObject("groupList",
                            this.groupService
                                    .getGroupsAccordingUnderCoordinator(
                                            ( (User) session.getAttribute( "thisUser" ) ).getUserId() ) )
                    .addObject("groupsBelongedTo",
                            this.groupService.getGroupsThatUserBelongsTo(
                                    (Integer)( ( User )session.getAttribute( "thisUser" )).getUserId()  ) );
        }
    }


}
