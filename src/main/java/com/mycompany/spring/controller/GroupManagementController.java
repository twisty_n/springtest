/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.spring.controller;

import com.mycompany.spring.models.EnhancedUser;
import com.mycompany.spring.models.MePoThread;
import com.mycompany.spring.models.ResearchGroup;
import com.mycompany.spring.models.User;
import com.mycompany.spring.service.ResearchGroupService;
import com.mycompany.spring.service.ThreadDocService;
import com.mycompany.spring.service.UserService;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author tristan
 */
@Controller
public class GroupManagementController {

    private UserService userService;
    private ResearchGroupService groupService;
    private ThreadDocService thradocService;

    @Autowired
    public void setThreadDocService ( ThreadDocService tds ) {
        this.thradocService = tds;
    }

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @Autowired
    public void setGroupSerice(ResearchGroupService groupService) {
        this.groupService = groupService;
    }

    /**
     * A view to edit the group information
     * @param groupId
     * @param request
     * @param session
     * @return
     */
    @RequestMapping( "coord/editGroup" )
    public ModelAndView editGroupEntry(
            @RequestParam(value = "group", required = true) Integer groupId,
            HttpServletRequest request,
            HttpSession session) {

        ResearchGroup g = this.groupService.retreiveResearchGroup( groupId );

        User thisUser = (( User ) session.getAttribute( "thisUser" ) );

        if ( g.getGroupCoordinator() != thisUser.getUserId() ) {
            //They are trying to access a group they shouldnt
            return new ModelAndView("common/bad-access");
        }
        List<User> ul = this.userService.retrieveAllUsers();
        List<EnhancedUser> ingroup =(this.userService.getAllEnhancedUsersInGroup(groupId));

        ModelAndView view = new ModelAndView( "coord/EditGroup" );
        session.setAttribute("group", g); view.addObject("userList", ul );
        session.setAttribute("inGroup", ingroup);

        MePoThread meeting = new MePoThread();

        //Thread ID will be null which is ok

        view.addObject("MePoThread", meeting );

        return view;
    }

    @RequestMapping(value = "/coord/submitEdit", method=RequestMethod.POST)
    public ModelAndView doEditGroup (HttpSession session, HttpServletRequest request) {

        String name = request.getParameter( "groupName" );
        String description = request.getParameter( "description" );
        Integer id = ( ( ResearchGroup ) session.getAttribute("group") ).getGroupId();
        session.removeAttribute( "group" );

        this.groupService.updateGroupDetails(name, description, id);

        return new ModelAndView("redirect:/coord/editGroup?group="+id);

    }

    @RequestMapping(value = "coord/submitUser", method = RequestMethod.POST)
    public String doAddNewGroupUser(
            @RequestParam(value = "user", required = true) Integer userid,
            HttpSession session) {

        Integer groupid = ( (ResearchGroup) session.getAttribute("group") ).getGroupId();
        this.groupService.addNewUserToGroup(userid, groupid);

        return "redirect:/coord/editGroup?group="+groupid;
    }

    @RequestMapping(value = "coord/delete-from-group", method = RequestMethod.POST)
    public String doDeleteFromGroup(
            @RequestParam(value = "uid") Integer userId,
            HttpSession session) {

        Integer groupId = ( (ResearchGroup) session.getAttribute("group") ).getGroupId();

        this.groupService.removePersonFromResearchGroup(userId, groupId);

        return "redirect:/coord/editGroup?group="+groupId;

    }

    @RequestMapping(value = "coord/view-access")
    public ModelAndView ViewAccessLog(
        HttpSession session) {

        ModelAndView view = new ModelAndView( "coord/view-access" );
        Integer groupId = ( (ResearchGroup) session.getAttribute("group") ).getGroupId();

        view.addObject("accessLog", this.thradocService.retrieveAccessRecords( groupId ) );

        return view;

    }


}
