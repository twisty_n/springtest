//TODO add decent header
package com.mycompany.spring.controller;

import com.mycompany.spring.models.ResearchGroup;
import com.mycompany.spring.models.User;
import com.mycompany.spring.service.ResearchGroupService;
import com.mycompany.spring.service.UserService;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

/**
 * Controls all of the requests for an admin user
 * @author tristan
 */
@Controller
public class AdminController {

    private UserService userService;

    private ResearchGroupService researchGroupService;

    @Autowired
    public void setUserService ( UserService userService ) {
        this.userService = userService;
    }

    @Autowired
    public void setResearchGroupService ( ResearchGroupService rgs ) {
        this.researchGroupService = rgs;
    }

    /**
     * Displays the initial view that the admin will see after logging in
     * @return
     */
    @RequestMapping(value = "/admin/admin-landing")
    public ModelAndView goToAdminLanding() {

        //
        ModelAndView view = new ModelAndView();
        view.setViewName( "admin/admin-landing" );
        return view;
    }

    @RequestMapping(value = "/admin/view-all")
    public ModelAndView goToViewAll () {

        //
        ModelAndView view = new ModelAndView();
        view.setViewName("admin/Overview");
        view.addObject ( "userList", this.userService.retrieveAllUsers() );
        view.addObject( "researchGroupList", researchGroupService.retreiveAllResearchGroups() );
        return view;
    }

    @RequestMapping(value = "/admin/user-operation")
    public ModelAndView goToUserOps () {

        ModelAndView view = new ModelAndView("admin/user-operation");
        view.addObject("user", new User() );
        view.addObject("userList", this.userService.retrieveAllUsers());

        return view;
    }

    @RequestMapping(value = "/admin/doCreateUser", method=RequestMethod.POST)
    public ModelAndView goToUserCreateStatus (
            Model model,
            @Valid User newUser,
            BindingResult result,
            @RequestParam(value="password", defaultValue="password") String password,
            @RequestParam(value = "role", required = true) Integer role) {

         if ( result.hasErrors() ) {
            return new ModelAndView( "admin/user-operation" );
        } else {

             try {
                this.userService.addUserToDatabase(newUser, password, role);
                return this.goToViewAll().addObject("status", "New user created" + newUser.getUserName());
             } catch ( DuplicateKeyException e ) {
                 //Duplicate username
                 return this.goToUserOps().addObject("unameDuplicate", "Username must be unique!" + newUser.getUserName() );
             }
        }
    }


    @RequestMapping(value = "/admin/doDeleteUser", method=RequestMethod.GET)
    public ModelAndView goToDeleteUser (
            @RequestParam(value = "userName", required = true) String userid)  {

        try {
            this.userService.removeUserFromSystem( userid );
        } catch ( DataIntegrityViolationException sql) {
            return this.goToUserOps().addObject("error", "Cannot Delete Group Coordinator");
        }
        //
        return this.goToViewAll();
    }


    /* Group Stuff */

    @RequestMapping(value = "/admin/group-operations")
    public ModelAndView goToGroupOps () {

        ModelAndView view = new ModelAndView("admin/group-operations");
        view.addObject("group", new ResearchGroup() );
        view.addObject("userList", this.userService.retrieveAllUsers());
        view.addObject("groupList", this.researchGroupService.retreiveAllResearchGroups());

        return view;
    }

    @RequestMapping(value = "/admin/doCreateRG", method = RequestMethod.POST)
    public ModelAndView doAddResearchGroup (
            Model model,
            @Valid ResearchGroup newRG,
            BindingResult result) {

        if ( result.hasErrors() ) {
            return new ModelAndView( "admin/group-operations" );
        } else {
            this.researchGroupService.addResearchGroupToSystem( newRG );
        }

        return this.goToViewAll();
    }

    @RequestMapping(value = "/admin/doDeleteRG", method = RequestMethod.GET)
    public ModelAndView doDeleteResearchGroup (
            @RequestParam( value = "groupId", required = true ) Integer groupId) {

        this.researchGroupService.removeResearchGroupFromSystem( groupId.intValue() );

        return this.goToViewAll();
    }

    @RequestMapping(value = "/admin/assign-coordinator")
    public ModelAndView selectNewCoordinator () {

        ModelAndView view = new ModelAndView( "admin/assign-coordinator" );
        view.addObject("", new Object() ); //Needed to shut the JSP compiler up
        view.addObject( "groupList", this.researchGroupService.retreiveAllResearchGroups() );
        view.addObject( "userList", this.userService.retrieveAllUsers() );

        return view;
    }

    @RequestMapping(value = "admin/doAssignNewCoordinator")
    public ModelAndView doAssignNewCoordinator(
            @RequestParam( value = "group", required = true) Integer group,
            @RequestParam( value = "newCoordinator", required = true ) Integer newCoordinator ) {

        this.researchGroupService.assignNewGroupCoordinator( group, newCoordinator );

        return this.goToViewAll();
    }


}
