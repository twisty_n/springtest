/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.spring.models;

import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

/**
 *  A container representing the state of a file upload
 * @author tristan
 */
@Configurable
@Component("PhotoProfileUploadContainer")
@Scope("session")
public class ProfilePhotoUploadContainer {

    private Integer userId;
    private String photoUrl;
    private MultipartFile file;

    public ProfilePhotoUploadContainer() {
        this.userId = null;
        this.photoUrl = null;
        this.file = null;
    }

    public ProfilePhotoUploadContainer(
            Integer userId,
            String photoUrl,
            MultipartFile file) {
        this.userId = userId;
        this.photoUrl = photoUrl;
        this.file = file;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public MultipartFile getFile() {
        return file;
    }

    public void setFile(MultipartFile file) {
        this.file = file;
    }



}
