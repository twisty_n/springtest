/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.spring.models;

import org.springframework.context.annotation.Scope;

/**
 *  Models a thread made in a discussion board
 * @author tristan
 */
@Scope("session")
public abstract class Thread {

    private String name;
    private Integer userId;
    private Integer groupId;
    private Integer threadId;
    private User threadCreator;

    private String dateCreated;

    /**
     * Default
     */
    public Thread () {
        this.threadId = null;
        this.name = null;
        this.userId = null;
        this.groupId = null;
        this.threadCreator = null;
    }

    public Thread ( Thread t ) {
        this.name = t.name;
        this.userId = t.userId;
        this.groupId = t.groupId;
        this.threadCreator = t.threadCreator;
    }

    /**
     * Intermediate Constructor
     * @param name
     */
    public Thread ( String name ) {
        this.threadId = null;
        this.name = name;
        this.userId = null;
        this.userId = null;
        this.threadCreator = null;
    }

    /**
     * Full constructor
     * @param threadId
     * @param name
     * @param userId
     * @param groupId
     * @param threadCreator
     */
    public Thread(
            Integer threadId,
            String name,
            Integer userId,
            Integer groupId,
            User threadCreator) {
        this.threadId = threadId;
        this.name = name;
        this.userId = userId;
        this.groupId = groupId;
        this.threadCreator = threadCreator;
    }


    /**
     * Get the value of dateCreated
     *
     * @return the value of dateCreated
     */
    public String getDateCreated() {
        return dateCreated;
    }

    /**
     * Set the value of dateCreated
     *
     * @param dateCreated new value of dateCreated
     */
    public void setDateCreated(String dateCreated) {
        this.dateCreated = dateCreated;
    }

    /**
     * Get the value of threadId
     *
     * @return the value of threadId
     */
    public Integer getThreadId() {
        return threadId;
    }

    /**
     * Set the value of threadId
     *
     * @param threadId new value of threadId
     */
    public void setThreadId(Integer threadId) {
        this.threadId = threadId;
    }

    /**
     *
     * @return name of thread
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return Then user Id
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * Sets the userId
     * @param userId
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * Gets the research group id for this group
     * @return
     */
    public Integer getGroupId() {
        return groupId;
    }

    /**
     *  Sets the group id
     * @param groupId
     */
    public void setGroupId(Integer groupId) {
        this.groupId = groupId;
    }

    /**
     * Gets the thread creator
     * @return
     */
    public User getThreadCreator() {
        return threadCreator;
    }

    /**
     * Sets a new thread creator object
     * @param threadCreator
     */
    public void setThreadCreator(User threadCreator) {
        this.threadCreator = threadCreator;
    }



}
