/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.spring.models;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 *  Encapsulates the extra information present in a user profile
 * @author tristan
 */
@Configurable
@Component("ProfileContainer") //This is a bean component to be used
@Scope("session")
//TODO MAY NEED TO ADD SCOPE THING
public class ProfileContainer {

    @NotNull
    @NotEmpty(message = "Password Required")
    private String password;

    @NotNull
    @Max(value = 2000)
    @NotEmpty(message = "Bio Required")
    private String description;

    private String staffPosition;

    private String studentDegree;

    public ProfileContainer() {
        this.password = null;
        this.description = null;
        this.staffPosition = null;
        this.studentDegree = null;
    }

    public ProfileContainer(
            String password,
            String description,
            String staffPosition,
            String studentDegree) {
        this.password = password;
        this.description = description;
        this.staffPosition = staffPosition;
        this.studentDegree = studentDegree;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStaffPosition() {
        return staffPosition;
    }

    public void setStaffPosition(String staffPosition) {
        this.staffPosition = staffPosition;
    }

    public String getStudentDegree() {
        return studentDegree;
    }

    public void setStudentDegree(String studentDegree) {
        this.studentDegree = studentDegree;
    }



}
