/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.spring.models;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Models a post to be made to a discussion area
 * @author tristan
 */
@Configurable
@Component("Post") //This is a bean component to be used
@Scope("session")
public class Post extends Thread{

    @NotNull
    @Size(min = 1, max = 10000, message = "Post must not be larger than 10000 characters")
    @NotEmpty(message = "Please fill in a group name")
    private String content;
    private User poster;
    private String datePosted;

    /**
     * Default Constructor
     */
    public Post() {
        this.content = null;
        this.poster = null;
    }

    /**
     * Default Constructor
     * @param content
     * @param poster
     */
    public Post(
            String content,
            User poster) {
        this.content = content;
        this.poster = poster;
    }

    /**
     * Prefered constructor ( Determines context )
     * @param content The content of the post
     * @param poster The user who created the post
     * @param t The Thread object that the post exists in
     */
    public Post(
            String content,
            User poster,
            Thread t ) {
        super(t);
        this.content = content;
        this.poster = poster;
    }


    /**
     * Get the value of datePosted
     *
     * @return the value of datePosted
     */
    public String getDatePosted() {
        return datePosted;
    }

    /**
     * Set the value of datePosted
     *
     * @param datePosted new value of datePosted
     */
    public void setDatePosted(String datePosted) {
        this.datePosted = datePosted;
    }


    /**
     * Get the value of content
     *
     * @return the value of content
     */
    public String getContent() {
        return content;
    }

    /**
     * Set the value of content
     *
     * @param content new value of content
     */
    public void setContent(String content) {
        this.content = content;
    }

     /**
     * Get the value of poster
     *
     * @return the value of poster
     */
    public User getPoster() {
        return poster;
    }

    /**
     * Set the value of poster
     *
     * @param poster new value of poster
     */
    public void setPoster(User poster) {
        this.poster = poster;
    }


}
