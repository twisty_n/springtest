package com.mycompany.spring.models;

import java.util.Collections;
import java.util.List;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

/**
 * A basic User model that can be extended and customized if needed
 *
 * @author Tristan
 */
@Configurable
@Component("UserModel") //This is a bean component to be used
@Scope(value="session", proxyMode=ScopedProxyMode.TARGET_CLASS) //Sets the beans lifetime
public class User {

    private int userId;

    @NotNull
    @NotEmpty(message = "Please provide a first name")
    @Size(min = 1, max = 30)
    private String firstName;

    @NotNull
    @NotEmpty(message = "Please provide a last name")
    @Size(min = 1, max = 30)
    private String lastName;

    @NotNull
    @Pattern(regexp = ".*\\@.*\\..*")
    @NotEmpty(message = "Please provide a valid email")
    @Size(min = 1, max = 30)
    private String emailAddress;

    @NotNull
    @NotEmpty(message = "Please provide a username")
    @Size(min = 1, max = 30)
    private String userName;

    //Id's of groups belonged to
    private  List<Integer> groupsBelongingTo;

    /**
     * Default Constructor
     * Will set up all items as null
     */
    public User() {
        this.userId = -1;
        this.firstName = null;
        this.lastName = null;
        this.emailAddress = null;
        this.userName = null;
        this.groupsBelongingTo = null;
    }

    /**
     * Preferred Constructor
     * @param userId
     * @param firstName
     * @param lastName
     * @param emailAddress
     * @param userName
     * @param groupsBelongingTo
     */
    public User(
            int userId,
            String firstName,
            String lastName,
            String emailAddress,
            String userName,
            List<Integer> groupsBelongingTo) {
        this.userId = userId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.emailAddress = emailAddress;
        this.userName = userName;
        this.groupsBelongingTo = groupsBelongingTo;
    }

    /**
     * Helper Constructor
     * @param userId
     * @param firstName
     * @param lastName
     * @param emailAddress
     * @param userName
     */
    public User(
            int userId,
            String firstName,
            String lastName,
            String emailAddress,
            String userName ) {
        this.userId = userId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.emailAddress = emailAddress;
        this.userName = userName;
        this.groupsBelongingTo = null;
    }

       /**
     * Helper Constructor
     * @param firstName
     * @param lastName
     * @param emailAddress
     * @param userName
     */
    public User(
            String firstName,
            String lastName,
            String emailAddress,
            String userName ) {
        this.userId = -1;
        this.firstName = firstName;
        this.lastName = lastName;
        this.emailAddress = emailAddress;
        this.userName = userName;
        this.groupsBelongingTo = null;
    }

    /**
     *
     * @return The id of the user
     */
    public int getUserId () {
        return this.userId;
    }

    public void setUserId ( int id ) {
        this.userId = id;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setGroupsBelongingTo(List<Integer> groupsBelongingTo) {
        this.groupsBelongingTo = groupsBelongingTo;
    }



    /**
     *
     * @return First name of user
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     *
     * @return Last name of user
     */
    public String getLastName() {
        return lastName;
    }

    /**
     *
     * @return Email address of user
     */
    public String getEmailAddress() {
        return emailAddress;
    }

    /**
     *
     * @return username of user
     */
    public String getUserName() {
        return userName;
    }

    /**
     *
     * @return List of Research Groups that the user belongs to
     */
    public List<Integer> getGroupsBelongingTo() {

        //A list so that the data model doies not get corrupt by any users of the API
        //This is ensured with unmodifiable
        return this.groupsBelongingTo;
    }

    @Override
    public String toString() {
        return "User{" + "userId=" + userId + ", firstName=" + firstName + ", lastName=" + lastName + ", emailAddress=" + emailAddress + ", userName=" + userName + ", groupsBelongingTo=" + groupsBelongingTo.toString() + '}';
    }

    public User getThis() {
        return this;
    }


}
