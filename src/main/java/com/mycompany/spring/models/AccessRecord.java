/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.spring.models;

import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Models and access record in teh system
 * @author tristan
 */
@Configurable
@Component("AccessRecord") //This is a bean component to be used
@Scope("session")
public class AccessRecord {

    private String threadName;
    private String dateAccessed;
    private User user;

    /**
     * Default Constructor
     */
    public AccessRecord () {
        this.dateAccessed = null;
        this.threadName = null;
        this.user =null;
    }

    public AccessRecord (
            String threadName,
            String dateAccessed,
            User user) {
        this.threadName = threadName;
        this.dateAccessed = dateAccessed;
        this.user = user;
    }

    /**
     * Get the value of user
     *
     * @return the value of user
     */
    public User getUser() {
        return user;
    }

    /**
     * Set the value of user
     *
     * @param user new value of user
     */
    public void setUser(User user) {
        this.user = user;
    }

    /**
     * Get the value of dateAccessed
     *
     * @return the value of dateAccessed
     */
    public String getDateAccessed() {
        return dateAccessed;
    }

    /**
     * Set the value of dateAccessed
     *
     * @param dateAccessed new value of dateAccessed
     */
    public void setDateAccessed(String dateAccessed) {
        this.dateAccessed = dateAccessed;
    }


    /**
     * Get the value of threadName
     *
     * @return the value of threadName
     */
    public String getThreadName() {
        return threadName;
    }

    /**
     * Set the value of threadName
     *
     * @param threadName new value of threadName
     */
    public void setThreadName(String threadName) {
        this.threadName = threadName;
    }


}
