/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.spring.models;

import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 *
 * @author tristan
 */
@Configurable
@Component("FiPoThread") //This is a bean component to be used
@Scope("session")
public class FiPoThread extends Post {

    private String filename;
    private String format;
    private Integer version;

    public FiPoThread() {
        this.filename = null;
        this.format = null;
        this.version = null;
    }

    public FiPoThread(
            String filename,
            String format,
            Integer string,
            String content,
            User poster,
            Thread t) {
        super(content, poster, t);
        this.filename = filename;
        this.format = format;
        this.version = string;
    }



    /**
     * Get the value of string
     *
     * @return the value of string
     */
    public Integer getVersion() {
        return version;
    }

    /**
     * Set the value of string
     *
     * @param string new value of string
     */
    public void setVersion(Integer string) {
        this.version = string;
    }


    /**
     * Get the value of format
     *
     * @return the value of format
     */
    public String getFormat() {
        return format;
    }

    /**
     * Set the value of format
     *
     * @param format new value of format
     */
    public void setFormat(String format) {
        this.format = format;
    }


    /**
     * Get the value of filename
     *
     * @return the value of filename
     */
    public String getFilename() {
        return filename;
    }

    /**
     * Set the value of filename
     *
     * @param filename new value of filename
     */
    public void setFilename(String filename) {
        this.filename = filename;
    }


}
