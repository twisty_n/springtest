/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.spring.models;

import java.util.List;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * An enhanced user model, with super speed and flight
 * @author Tristan
 */
@Configurable
@Component("EnhancedUser") //This is a bean component to be used
@Scope("session")
public class EnhancedUser extends User {

    private String photoUrl;
    private String description;
    private String staffPos;
    private String studentDegree;

    public EnhancedUser(
            String photoUrl,
            String description,
            String staffId,
            String studentId) {
        this.photoUrl = photoUrl;
        this.description = description;
        this.staffPos = staffId;
        this.studentDegree = studentId;
    }

    public EnhancedUser(
            String photoUrl,
            String description,
            String staffId,
            String studentId,
            int userId,
            String firstName,
            String lastName,
            String emailAddress,
            String userName,
            List<Integer> groupsBelongingTo) {
        super(userId, firstName, lastName, emailAddress, userName, groupsBelongingTo);
        this.photoUrl = photoUrl;
        this.description = description;
        this.staffPos = staffId;
        this.studentDegree = studentId;
    }

    public EnhancedUser(
            String photoUrl,
            String description,
            String staffId,
            String studentId,
            int userId,
            String firstName,
            String lastName,
            String emailAddress,
            String userName) {
        super(userId, firstName, lastName, emailAddress, userName);
        this.photoUrl = photoUrl;
        this.description = description;
        this.staffPos = staffId;
        this.studentDegree = studentId;
    }

    public EnhancedUser(
            String photoUrl,
            String description,
            String staffId,
            String studentId,
            String firstName,
            String lastName,
            String emailAddress,
            String userName) {
        super(firstName, lastName, emailAddress, userName);
        this.photoUrl = photoUrl;
        this.description = description;
        this.staffPos = staffId;
        this.studentDegree = studentId;
    }

    public EnhancedUser() {
        super();
        this.photoUrl = null;
        this.description = null;
        this.staffPos = null;
        this.studentDegree = null;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStaffPos() {
        return staffPos;
    }

    public void setStaffPos(String staffPos) {
        this.staffPos = staffPos;
    }

    public String getStudentId() {
        return studentDegree;
    }

    public void setStudentId(String studentDegree) {
        this.studentDegree = studentDegree;
    }

    @Override
    public int getUserId() {
        return super.getUserId();
    }

    @Override
    public String toString() {
        return "EnhancedUser{"
                + super.toString()
                + "photoUrl=" + photoUrl + ", "
                + "description=" + description
                + ", staffId=" + staffPos
                + ", studentId=" + studentDegree + '}';
    }



}
