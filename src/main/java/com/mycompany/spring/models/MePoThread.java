/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.spring.models;

import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 *
 * @author tristan
 */
@Configurable
@Component("MePoThread") //This is a bean component to be used
@Scope("session")
public class MePoThread extends Post {

    private String location;
    private String agenda;
    private String dateAndTime;

    public MePoThread() {
        this.agenda = null;
        this.dateAndTime = null;
        this.location = null;
    }

    public MePoThread(
            String location,
            String agenda,
            String dateAndTime,
            String content,
            User poster,
            Thread t) {
        super(content, poster, t);
        this.location = location;
        this.agenda = agenda;
        this.dateAndTime = dateAndTime;
    }



    /**
     * Get the value of dateAndTime
     *
     * @return the value of dateAndTime
     */
    public String getDateAndTime() {
        return dateAndTime;
    }

    /**
     * Set the value of dateAndTime
     *
     * @param dateAndTime new value of dateAndTime
     */
    public void setDateAndTime(String dateAndTime) {
        this.dateAndTime = dateAndTime;
    }

    /**
     * Get the value of agenda
     *
     * @return the value of agenda
     */
    public String getAgenda() {
        return agenda;
    }

    /**
     * Set the value of agenda
     *
     * @param agenda new value of agenda
     */
    public void setAgenda(String agenda) {
        this.agenda = agenda;
    }

    /**
     * Get the value of location
     *
     * @return the value of location
     */
    public String getLocation() {
        return location;
    }

    /**
     * Set the value of location
     *
     * @param location new value of location
     */
    public void setLocation(String location) {
        this.location = location;
    }

}
