/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.spring.models;

import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author tristan
 */
@Configurable
@Component("DocumentUploadContainer")
@Scope("session")
public class DocumentUpLoadContainer extends FiPoThread {

    private MultipartFile file;

    public DocumentUpLoadContainer() {
        this.file = null;
    }

    public DocumentUpLoadContainer( MultipartFile file, String fileName ) {
        this.file = file;
    }

    /**
     * Get the value of file
     *
     * @return the value of file
     */
    public MultipartFile getFile() {
        return file;
    }

    /**
     * Set the value of file
     *
     * @param file
     */
    public void setFile(MultipartFile file) {
        this.file = file;
    }

}
