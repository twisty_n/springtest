//TODO add decent header
package com.mycompany.spring.models;

import java.util.List;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

/**
 * A basic model to represent and encapsulate a research group
 * TODO: Modify to final DB
 * Fields set as final to avoid corruption
 *
 * @author tristan
 */
@Configurable
@Component("ResearchGroupModel") //This is a bean component to be used
@Scope("session")
public class ResearchGroup {

    private final int groupId;

    @NotNull
    @Size(min = 1, max = 30)
    @NotEmpty(message = "Please fill in a group name")
    private  String groupName;

    private  String groupDescription;

    private int groupCoordinator;

    private  List<Integer> groupMembers;

    //TODO remove groupId if not needed

    /**
     * Default constructor-Creates a useless bean
     */
    public ResearchGroup () {
        this.groupId = -1;
        this.groupName = null;
        this.groupMembers = null;
        this.groupCoordinator = -1;
        this.groupDescription = null;
    }

    /**
     * Standard constructor
     * @param groupId The groups unique id
     * @param groupName Name of the research group
     * @param groupDescription Description of the research group
     * @param groupCoordinator
     * @param groupMembers  <List> of people in the research group
     */
    public ResearchGroup (
            int groupId,
            String groupName,
            String groupDescription,
            int groupCoordinator,
            List<Integer> groupMembers) {
        this.groupId = groupId;
        this.groupName = groupName;
        this.groupDescription = groupDescription;
        this.groupCoordinator = groupCoordinator;
        this.groupMembers = groupMembers;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public void setGroupDescription(String groupDescription) {
        this.groupDescription = groupDescription;
    }

    public void setGroupMembers(List<Integer> groupMembers) {
        this.groupMembers = groupMembers;
    }

    public int getGroupCoordinator() {
        return groupCoordinator;
    }

    public void setGroupCoordinator(int groupCoordinator) {
        this.groupCoordinator = groupCoordinator;
    }

    /**
     *
     * @return The unique group Id for the group
     */
    public int getGroupId() {
        return groupId;
    }

    /**
     *
     * @return String name of the group
     */
    public String getGroupName() {
        return groupName;
    }

    /**
     *
     * @return Description of the group
     */
    public String getGroupDescription() {
        return groupDescription;
    }

    /**
     * All of the ids of the current members of the research group
     * @return <List> of User group members
     */
    public List<Integer> getGroupMembers() {
        return groupMembers;
    }

    @Override
    public String toString() {

        //
        String rgString = "ResearchGroup{"
                + "groupName= "
                + groupName
                + ", groupDescription= "
                + groupDescription
                + ", groupMembers= ";

        for (int i = 0; i < groupMembers.size(); i++) {
            rgString += groupMembers.get(i).toString();
            rgString += ", ";
        }
        rgString += " coordinator=" + this.groupCoordinator + "}";
        return  rgString;
    }




}
