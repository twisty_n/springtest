package com.mycompany.spring.init;

import com.mycompany.spring.service.FullUserEntityService;
import java.util.Properties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.security.web.access.expression.DefaultWebSecurityExpressionHandler;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.handler.SimpleMappingExceptionResolver;
import org.springframework.web.servlet.i18n.CookieLocaleResolver;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;
import org.springframework.web.servlet.view.JstlView;
import org.springframework.web.servlet.view.UrlBasedViewResolver;

//TODO add support and imports for the suecurity module when it has been writtne and implemented

/**
 *This configuration class provides support to avoid the use of an application context XML file :D
 * @author Tristan
 */
@Configuration
@ComponentScan( "com.mycompany" ) //Specfies which packages to scan
@EnableWebMvc // Enables the use of annotations
@ImportResource( "classpath:spring-security.xml" )
public class WebAppConfig extends WebMvcConfigurerAdapter{

    /**
     * Required to define where the JPS's are stored so that they can be retrieved
     * The view resolver translates the " return 'someView' " into an actual view
     * as it sits in WEB-INF/pages -> they are mapped by name
     * @return
     */
    @Bean
    public UrlBasedViewResolver setupViewResolver() {

        //Sets up and allowws access to the view components in the locked down WEB-INF folder
        UrlBasedViewResolver resolver = new UrlBasedViewResolver();
        resolver.setPrefix("/WEB-INF/pages/");
        resolver.setSuffix(".jsp");
        resolver.setViewClass(JstlView.class);
        return resolver;
    }

    /**
     * Allows access to the resources (scripts, CSS) in the resources folder
     * @param registry
     */
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/resources/**").addResourceLocations("/resources/");
    }

  /**
   * Sets up our request interceptors. This is a mechanism that allows us do
   * things to the request before it reaches the controller
   * E.g. We could implement some security filtering or whatever
   * @param registry
   */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {

        LocaleChangeInterceptor localeChangeInterceptor = new LocaleChangeInterceptor();
        localeChangeInterceptor.setParamName("lang");
        registry.addInterceptor(localeChangeInterceptor);
    }

    /**
     * This is to allow support for multiple languages
     * @return
     */
    @Bean
    public LocaleResolver localeResolver() {

        CookieLocaleResolver cookieLocaleResolver = new CookieLocaleResolver();
        cookieLocaleResolver.setDefaultLocale(StringUtils.parseLocaleString("en"));
        return cookieLocaleResolver;
    }

    /**
     * Sets up the database access and configuration information
     * TODO Change the datasource from this in efficient one
     * @return
     */
    @Bean
    public DriverManagerDataSource dataSource() {

        //TODO Adjust these values to match our testing DB

        //Set up our access information
        String dbName = "research"; //The name of the database you want to access
        String driverClassName = "com.mysql.jdbc.Driver"; //The name of the driver class to use
        String url = "jdbc:mysql://localhost:3306/" + dbName; //The URL that the database is located at
        String username ="tristan"; //The username to access the DB -> ensure it has persmissions
        String password = "zx22219499";

        //Create out new datasource with the info
        DriverManagerDataSource d = new DriverManagerDataSource();
        d.setDriverClassName(driverClassName);
        d.setUrl(url);
        d.setUsername(username);
        d.setPassword(password);

        return d; //The best kind of return
    }

    /**
     * Sets up our user entity valitor bean thing
     * TODO Remove this if not needed
     * @return
     */
    @Bean
    public FullUserEntityService fullUserEntityService() {

        return new FullUserEntityService();
    }

    @Bean
    public DefaultWebSecurityExpressionHandler webHandleGen() {
        DefaultWebSecurityExpressionHandler wg = new DefaultWebSecurityExpressionHandler();

        return wg;
    }

    @Bean
    public CommonsMultipartResolver multipartResolver () {
        CommonsMultipartResolver cmr = new CommonsMultipartResolver();
        cmr.setMaxUploadSize(10000000);
        return cmr;
    }
//
    @Bean
    public org.springframework.mail.javamail.JavaMailSenderImpl mailSender () {
        JavaMailSenderImpl jms = new JavaMailSenderImpl();
        jms.setHost( "smtp.gmail.com" );
        jms.setPort( 25 );
        jms.setUsername("serverservington@gmail.com");
        jms.setPassword("qazdrtgb");
        Properties p = new Properties();
        p.setProperty("mail.transport.protocol", "smtp");
        p.setProperty("mail.smtp.auth", "true");
        p.setProperty("mail.smtp.starttls.enable", "true");
        jms.setJavaMailProperties(p);

        return jms;
    }

}