/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.spring.init;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration.Dynamic;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

/**
 * Avoids the requirement for XML-based configuration
 *
 * @author Tristan
 */
@Configuration
public class Initializer implements WebApplicationInitializer {

    @Override
    public void onStartup(ServletContext servletContext) throws ServletException {

	//Establish our contexts and obtain the configuration classes
	AnnotationConfigWebApplicationContext ctx = new AnnotationConfigWebApplicationContext();
	ctx.register(WebAppConfig.class);

	servletContext.addListener(new ContextLoaderListener(ctx));

	ctx.setServletContext(servletContext);

	//Set up our dispatcher Servlet
	Dynamic servlet = servletContext.addServlet("dispatcher", new DispatcherServlet(ctx));
	servlet.addMapping("/");
	servlet.setLoadOnStartup(1);

    }
}
