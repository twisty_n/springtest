package com.mycompany.spring.dao;

import java.sql.SQLException;
import java.util.List;
import javax.sql.DataSource;

/**
 * Provides a standard interface for Data Access Objects to implement
 * With a set of the most basic methods needed to function as a DAO
 * @author Tristan
 * @param <T> The type of DAO that the inferface will address
 */
public interface DataAccessObject<T> {

    /**
     * Set up the datasource that the DAO will use to extract data from the database
     * Also sets up our JDBC template using the set datasource
     * @param dataSource
     */
    public void setDataSource(DataSource dataSource);

    /**
     * Add a model to the database ( As pertaining to the implementing DAO )
     * @param dataModel The model to add into the database
     * @throws java.sql.SQLException
     */
    public void addToDb(T dataModel) throws SQLException;

    /**
     * Remove a specified model from the database ( As pertaining to the implementing DAO )
     * @param <S>
     * @throws java.sql.SQLException
     */
    public <S> void removeFromDb(S identifer) throws SQLException;

    /**
     * Retrieves a specific model from the database using a provided identifier
     * @param <S> The type of identifier that will be used
     * @param identifier The identifier to use
     * @return The model matching the specified identifier
     * @throws java.sql.SQLException
     */
    public <S> T getModel(S identifier) throws SQLException;

    /**
     * Retrieves all of the models in the database that match the specified type
     * @return A List of the models of the specified type
     * @throws java.sql.SQLException
     */
    public List<T> getModels() throws SQLException;



}
