/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.spring.dao;

import com.mycompany.spring.models.ResearchGroup;
import com.mycompany.spring.service.UserService;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.RowMapperResultSetExtractor;
import org.springframework.stereotype.Repository;

/**
 *
 * @author tristan
 */
@Repository /* Yet another supository of wisdom */
public class ResearchGroupDAO implements  DataAccessObject<ResearchGroup>{

    private DataSource dataSource;
    private JdbcTemplate jto;
    private UserService userService;


    private class ResearchGroupRowMapper implements RowMapper<ResearchGroup> {

        @Override
        public ResearchGroup mapRow ( ResultSet rs, int i ) throws SQLException {

            ResearchGroup researchGroup = new ResearchGroup(
                rs.getInt( "id" ),
                rs.getString( "name" ),
                rs.getString( "description" ),
                rs.getInt( "coordinator" ),
                userService.getUsersInGroup ( rs.getInt( "id" ) ) );

            return researchGroup;
        }

    }

    private class ResearchGroupIdRowMapper implements RowMapper<Integer> {

        @Override
        public Integer mapRow ( ResultSet rs, int i ) throws SQLException {

            Integer researchGroupId = new Integer(
                rs.getInt( "id" ) );

            return researchGroupId;
        }

    }


    @Override
    @Autowired
    public void setDataSource(DataSource dataSource) {

        //
        this.dataSource = dataSource;
        this.jto = new JdbcTemplate(dataSource);
    }

    @Autowired
    public void setUserService ( UserService userService ) {
        this.userService = userService;
    }


    /**
     * Adds a research group to the database
     * @param dataModel
     */
    @Override
    public void addToDb(ResearchGroup dataModel) {

        //
        String query = ""
                + "INSERT INTO researchgroup"
                + "("
                + "name,"
                + "description,"
                + "coordinator )"
                + "VALUES ( ?, ?, ? );";

        jto.update(
                query,
                new Object[] {
                dataModel.getGroupName(),
                dataModel.getGroupDescription(),
                dataModel.getGroupCoordinator() } );

        //New functionality to auto-add the coord to the group

        int groupId = jto.queryForInt( "SELECT id FROM researchgroup ORDER BY id DESC LIMIT 1;" );

        this.userService.elevateToGC( dataModel.getGroupCoordinator() );

        this.createGroupPersonMapping( dataModel.getGroupCoordinator(), groupId );

    }

    /**
     * Removes a research group from the database
     * @param <Integer>
     * @param id
     */
    @Override
    public <Integer> void removeFromDb(Integer id) {

        //TODO Delete belong from here <<<<<<------------------------------
         String query0 = ""
                + "DELETE FROM belong "
                + "WHERE researchgroupid = ?;";

         jto.update(
                query0,
                new Object[] { id } );

         String query = ""
                + "DELETE FROM threadoc "
                + "WHERE researchgroupid = ?;";

         jto.update(
                query,
                new Object[] { id });
//
        String query1 = ""
                + "DELETE FROM "
                + "researchgroup WHERE  "
                + "id = ?";
        
//Dabsons amazing gigaquery that didn't work :/
//        String query = ""
//                + "DELETE threadoc, belong, file, meeting, post, accessed "
//                + "FROM researchgroup "
//                + "JOIN threadoc ON researchgroup.id = threadoc.researchgroupid "
//                + "JOIN belong ON researchgroup.id = belong.researchgroupid "
//                + "JOIN file on threadoc.fileid = file.id "
//                + "JOIN meeting on threadoc.meetingid = meeting.id "
//                + "JOIN post on post.threadocid = threadoc.id "
//                + "JOIN accessed on accessed.threadocid = threadoc.id "
//                + "WHERE researchgroup.id = ?;";
//
         jto.update(
                query1,
                new Object[] { id });



    }

    /**
     * Retrieves a research group using the specified id
     * @param <Integer>
     * @param identifier
     * @return
     */
    @Override
    public <Integer> ResearchGroup getModel(Integer identifier) {

        //
        java.lang.String query = ""
                + "SELECT "
                + "id, "
                + "name, "
                + "description, "
                + "coordinator "
                + "FROM researchgroup "
                + "WHERE id = ?;";

        ResearchGroup model = jto.queryForObject(
                query,
                new Object[] { identifier },
                new ResearchGroupRowMapper() );

        return model;
    }

    @Override
    public List<ResearchGroup> getModels() {

        //TODO fix this up to use a list based mapper

        //
        String query = ""
                + "SELECT "
                + "id, "
                + "name, "
                + "description, "
                + "coordinator "
                + "FROM researchgroup;";

        List<ResearchGroup> groupModels = jto.query(
                query,
                new RowMapperResultSetExtractor<ResearchGroup>(
                    new ResearchGroupRowMapper() ) );

        return groupModels;
    }

    public List<ResearchGroup> getModels( Object coordId ) {

        //TODO fix this up to use a list based mapper

        //
        String query = ""
                + "SELECT "
                + "id, "
                + "name, "
                + "description, "
                + "coordinator "
                + "FROM researchgroup "
                + "WHERE coordinator = ?;";

        List<ResearchGroup> groupModels = jto.query(
                query,
                new Object[] { coordId },
                new RowMapperResultSetExtractor<ResearchGroup>(
                    new ResearchGroupRowMapper() ) );

        return groupModels;
    }

    public List<Integer> getGroupsInUser ( int userId ) {

        //
        String query = ""
                + "SELECT "
                + "researchgroup.id "

                + "FROM researchgroup "
                + "INNER JOIN belong ON researchgroup.id=belong.researchgroupid\n"
                + "INNER JOIN user ON belong.userid=user.id\n"
                + "WHERE userid=?;";

        List<Integer> groupModels = jto.query(
                query,
                new Object[] { ( ( Integer ) userId ).toString() },
                new RowMapperResultSetExtractor<Integer>(
                    new ResearchGroupIdRowMapper() ) );

        return groupModels;
    }

    public List<ResearchGroup> getGroupsInUser ( Integer userId ) {

        //
        String query = ""
                + "SELECT "
                + "researchgroup.id,"
                + "researchgroup.name, "
                + "researchgroup.description, "
                + "researchgroup.coordinator  "
                + "FROM researchgroup "
                + "INNER JOIN belong ON researchgroup.id=belong.researchgroupid\n"
                + "INNER JOIN user ON belong.userid=user.id\n"
                + "WHERE userid=?;";

        List<ResearchGroup> groupModels = jto.query(
                query,
                new Object[] { ( ( Integer ) userId ).toString() },
                new RowMapperResultSetExtractor<ResearchGroup>(
                    new ResearchGroupRowMapper() ) );

        return groupModels;
    }

    public void assignNewCoordinator (Integer groupId, Integer userId) {

        //
        String query = ""
                + "UPDATE researchgroup "
                + "SET "
                + "coordinator = ? "
                + "WHERE "
                + "id = ?;";

        jto.update(
                query,
                new Object[] { userId, groupId } );

        this.createGroupPersonMapping( userId, groupId );

        this.userService.elevateToGC( userId );


    }

    public void createGroupPersonMapping ( Integer userId, Integer groupId ) {

        //
        String query = ""
                + "INSERT INTO "
                + "belong "
                + "VALUES ( ?, ? );";

        try {
            jto.update(
                    query,
                    new Object[] { userId, groupId } );

        } catch ( DuplicateKeyException dke ) {
            //Don't care, the user already exists in the DB :P
            //Ergo, we dont need to do anything
        }

    }

    public void updateGroupInformation (String name, String description, int id) {

        String query = ""
                + "UPDATE researchgroup "
                + "SET "
                + "name = ?, "
                + "description = ? "
                + "WHERE "
                + "id = ?;";

        jto.update(query,
                new Object[] {
                    name,
                    description,
                    id
                });
    }

    public void removePersonFromGroup ( Integer userid, Integer groupId ) {

        String query = ""
                + "DELETE FROM belong WHERE userid = ? AND researchgroupid = ?";

        jto.update(
                query,
                new Object[] { userid, groupId } );

    }

}
