/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.spring.dao;

import com.mycompany.spring.models.AccessRecord;
import com.mycompany.spring.models.FiPoThread;
import com.mycompany.spring.models.Thread;
import com.mycompany.spring.models.MePoThread;
import com.mycompany.spring.models.Post;
import com.mycompany.spring.models.User;
import com.mycompany.spring.service.UserService;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.RowMapperResultSetExtractor;
import org.springframework.stereotype.Repository;

/**
 * Provides database operations for group meeting/file/thread operations
 * @author tristan
 */
@Repository
public class ThreadDocDAO implements SpecialDataAccessObject{

    private DataSource dataSource;
    private JdbcTemplate jto;
     private UserService userService;

    @Autowired
    @Override
    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
        this.jto = new JdbcTemplate(dataSource);
    }

    @Autowired
    public void setUserService ( UserService userService ) {
        this.userService = userService;
    }

    /**
     *
     * @param mePoThread
     */
    public void createMeeting ( MePoThread mePoThread ) {

        String query = ""
                + "INSERT INTO meeting "
                + "VALUES ("
                + "NULL, ?, ?, ?);";

        //Create our meeting
        jto.update(
                query,
                new Object[] {
                    mePoThread.getDateAndTime(),
                    mePoThread.getLocation(),
                    mePoThread.getAgenda() } );

        String getMeetId = "(SELECT id FROM meeting ORDER BY id DESC LIMIT 1)";
        String nullString = "NULL";

        //Create out thread
        this.createThread(
                mePoThread,
                new Integer(1),
                getMeetId,
                nullString);

        String threadocId = "(SELECT id FROM threadoc ORDER BY id DESC LIMIT 1)";

        //Create our post
        this.createPost(mePoThread, threadocId);
    }

    /**
     * Standard discussion board post
     * @param p
     */
    public void createDiscussionThread( Post p ) {

        String nullString = "NULL";

        //Create out thread
        this.createThread(
                p,
                new Integer(0),
                nullString,
                nullString);

        String threadocId = "(SELECT id FROM threadoc ORDER BY id DESC LIMIT 1)";

        this.createPost(p, threadocId);

    }

    /**
     *
     * @param thread
     * @param threadType
     * @param isThreadMeet
     * @param isThreadFile
     */
    public void createThread ( Thread thread, Integer threadType,
            String isThreadMeet, String isThreadFile ) {

        String query = ""
                + "INSERT INTO threadoc "
                + "VALUES ("
                + "NULL, ?, ?, ( SELECT NOW() ), ?, ?,  "+
                isThreadMeet+" ,  "+
                isThreadFile+"  );";

        jto.update(
                query,
                new Object[] {
                    threadType,
                    thread.getName(),
                    thread.getUserId(),
                    thread.getGroupId() });

    }

    /**
     * If using query to get threadoc ID. ensure you have brackets around the SELECT statement
     * @param post
     * @param threadocId
     */
    public void createPost ( Post post, Object threadocId ) {

        String query = "INSERT INTO post "
                + "VALUES( NULL,"
                + " ( SELECT NOW() ),"
                + " ?, ?, "+
                threadocId+" );";

        jto.update(query,
                new Object[] {
                    post.getContent(),
                    post.getPoster().getUserId() } );

    }

    private class FileIdMapper implements RowMapper<Integer> {

        @Override
        public Integer mapRow(ResultSet rs, int i) throws SQLException {
            return new Integer( rs.getInt( "id" ) );
        }

    }

    public Integer createFileRecord ( FiPoThread fpt ) {

        String query = "INSERT INTO file VALUES"
                + "( "
                + "NULL, "
                + "?, ?, ? );";

        //Create our file record
        jto.update(
                query,
                new Object[] {
                    fpt.getFilename(),
                    fpt.getFormat(),
                    fpt.getVersion() } );

        String getFileId = "(SELECT id FROM file ORDER BY id DESC LIMIT 1)";

        //Retrieve the unique file id
        Integer uniqueFileIdentifer = jto.queryForObject(
                getFileId,
                new FileIdMapper() );

         jto.update(
                "UPDATE file SET filename = ? WHERE id = ?",
                uniqueFileIdentifer+"-" + fpt.getThreadCreator().getUserName()+ "-" + fpt.getFilename(),
                uniqueFileIdentifer );

        String nullString = "NULL";

        //Create our thread
        this.createThread(
                fpt,
                new Integer(2), //means its a thread document
                nullString,
                getFileId);

        String threadocId = "(SELECT id FROM threadoc ORDER BY id DESC LIMIT 1)";

        this.createPost( fpt, threadocId );

        return uniqueFileIdentifer;

    }

    public List<MePoThread> retrieveMeetingList ( Integer groupId ) {

        //
        String query = ""
                + "SELECT "
                + "threadoc.id AS threadId, "
                + "threadoc.name AS name, "
                + "meeting.date AS date, "
                + "meeting.location AS location "
                + "FROM meeting "
                + "INNER JOIN threadoc ON meeting.id = threadoc.meetingid "
                + "WHERE threadoc.type = 1 AND threadoc.researchgroupid = ?";

        List<MePoThread> meetingList = jto.query(
                query,
                new Object[] { groupId },
                new RowMapperResultSetExtractor<MePoThread>(
                    new MeetingListMeetRowMapper() ) );

        return meetingList;

    }

    public List<FiPoThread> retrieveFileThreadList ( Integer groupId ) {

        //
        String query = ""
                + "SELECT "
                + "threadoc.id AS threadId, "
                + "threadoc.name AS tname, "
                + "file.filename AS name, "
                + "file.version AS version, "
                + "file.format AS format,"
                + "threadoc.userid AS userid "
                + "FROM file "
                + "INNER JOIN threadoc ON file.id = threadoc.fileid "
                + "WHERE threadoc.type = 2 AND threadoc.researchgroupid = ?;";

        return jto.query(
                query,
                new Object[] { groupId },
                new RowMapperResultSetExtractor<FiPoThread>(
                    new FileListRowMapper() ) );

    }

    public List<Post> retriveDiscussionThreadList ( Integer groupId ) {

        //
        String query = "SELECT "
                + "id, name, date, userid "
                + "FROM threadoc "
                + "WHERE type = 0 AND researchgroupid = ?";

        return jto.query(
                query,
                new Object[] { groupId },
                new RowMapperResultSetExtractor<Post>(
                    new ThreadRowMapper()) );
    }

    public List<Post> retrievePostsInThread ( Integer threadId ) {

        //
        String query = ""
                + "SELECT "
                + "message AS content, "
                + "userid AS poster, "
                + "date AS dateposted "
                + "FROM post "
                + "WHERE threadocid = ?;";

        return jto.query(
                query,
                new Object[] { threadId },
                new RowMapperResultSetExtractor<Post>(
                    new PostMapper() ) );

    }

    public Post retrieveThreadDetails ( Integer threadId ) {

        //
        String query = ""
                + "SELECT "
                + "name, "
                + "date, "
                + "userid, "
                + "researchgroupid "
                + "FROM threadoc "
                + "WHERE id = ?";

        return jto.queryForObject(
                query,
                new Object[] { threadId },
                new SecureThreadRowMapper() );

    }

    public void  createAccessRecord (Integer threadId, Integer userid) {

        //
        String query = "INSERT INTO accessed VALUES ( ?, ?, ( SELECT NOW() ) );";

        jto.update(
                query,
                new Object[] { userid, threadId } );

    }

    public List<AccessRecord> retrieveAccessRecords ( Integer groupId ) {

        //
        String query = ""
                + "SELECT "
                + "accessed.userid AS userid,"
                + "threadoc.name AS name,"
                + "accessed.date AS date "
                + "FROM threadoc "
                + "INNER JOIN accessed ON threadoc.id = accessed.threadocid "
                + "WHERE threadoc.researchgroupid = ?;";

        return jto.query(
                query,
                new Object[]{groupId},
                new RowMapperResultSetExtractor<AccessRecord>(
                        new AccessRecordRowMapper() ) );

    }

    private class AccessRecordRowMapper implements RowMapper<AccessRecord> {

        @Override
        public AccessRecord mapRow(ResultSet rs, int i) throws SQLException {
           AccessRecord ar = new AccessRecord(
                   rs.getString( "date" ),
                   rs.getString( "name" ),
                   userService.getUserById( rs.getInt( "userid" ) ) );

           return ar;
        }

    }


    private class PostMapper implements RowMapper<Post> {

        @Override
        public Post mapRow(ResultSet rs, int i) throws SQLException {
            Post p = new Post(
                rs.getString( "content" ),
                userService.getUserById( rs.getInt( "poster" ) ));
            p.setDatePosted( rs.getString( "dateposted" ) );
            return p;
        }

    }

    private class ThreadRowMapper implements RowMapper<Post> {

        @Override
        public Post mapRow(ResultSet rs, int i) throws SQLException {
            Post p = new Post();
            p.setThreadId( rs.getInt( "id" ) );
            p.setName( rs.getString( "name" ) );
            p.setDateCreated( rs.getString( "date" ) );
            p.setThreadCreator( userService.getUserById( rs.getInt( "userid" ) ) );
            return p;
        }

    }

    private class SecureThreadRowMapper implements RowMapper<Post> {

        @Override
        public Post mapRow(ResultSet rs, int i) throws SQLException {
            Post p = new Post();
            p.setName( rs.getString( "name" ) );
            p.setDateCreated( rs.getString( "date" ) );
            p.setThreadCreator( userService.getUserById( rs.getInt( "userid" ) ) );
            p.setGroupId( rs.getInt( "researchgroupid" ) );
            return p;
        }

    }

    private class FileListRowMapper implements RowMapper<FiPoThread> {

        @Override
        public FiPoThread mapRow(ResultSet rs, int i) throws SQLException {

            FiPoThread fiInList = new FiPoThread();
            User threadCreator = userService.getUserById( rs.getInt( "userid" ) );
            fiInList.setFilename( rs.getString( "name" ) );
            fiInList.setFormat( rs.getString( "format" ) );
            fiInList.setVersion( rs.getInt( "version" ) );
            fiInList.setThreadId( rs.getInt( "threadId" ) );
            fiInList.setThreadCreator( threadCreator );
            fiInList.setName( rs.getString( "tname" ) );

            return fiInList;
        }

    }


    private class MeetingListMeetRowMapper implements RowMapper<MePoThread> {

        @Override
        public MePoThread mapRow ( ResultSet rs, int i ) throws SQLException {

            MePoThread meetInList = new MePoThread();
            meetInList.setThreadId( rs.getInt( "threadId" ) );
            meetInList.setName( rs.getString( "name" ) );
            meetInList.setDateAndTime( rs.getString( "date" ) );
            meetInList.setLocation( rs.getString( "location" ) );

            return meetInList;
        }

    }





}
