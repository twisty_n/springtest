/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.spring.dao;

import javax.sql.DataSource;

/**
 *
 * @author Tristan
 */
public interface SpecialDataAccessObject {
    
    /**
     * Set up the datasource that the DAO will use to extract data from the database
     * Also sets up our JDBC template using the set datasource
     * @param dataSource
     */
    public void setDataSource(DataSource dataSource);
    
}
