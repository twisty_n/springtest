package com.mycompany.spring.dao;

import com.mycompany.spring.models.EnhancedUser;
import com.mycompany.spring.models.ProfileContainer;
import com.mycompany.spring.models.User;
import com.mycompany.spring.service.ResearchGroupService;
import com.mycompany.spring.utils.FullUserEntity;
import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.RowMapperResultSetExtractor;
import org.springframework.stereotype.Repository;

/**
 * Performs all of the database operations relating to the user object
 * Add more operations as needed
 * @author Tristan
 */
@Repository
public class UserDAO implements DataAccessObject<User> {

    private DataSource dataSource;
    private JdbcTemplate jto;
    private ResearchGroupService rgService;


    private class UserRowMapper implements RowMapper<User> {

        @Override
        public User mapRow(ResultSet rs, int i) throws SQLException {

            User user = new User(
                rs.getInt( "id" ),
                rs.getString("fname"),
                rs.getString("lname"),
                rs.getString("email"),
                rs.getString("uname"),
                rgService.retreiveGroupsMappedToUser( rs.getInt( "id" ) ) );

            return user;
        }

    }

    private class UserIdRowMapper implements RowMapper<Integer> {

        @Override
        public Integer mapRow(ResultSet rs, int i) throws SQLException {

            Integer id = new Integer(
                rs.getInt( "id" ) );

            return id;
        }

    }


    @Override
    @Autowired
    public void setDataSource(DataSource dataSource) {

        //
        this.dataSource = dataSource;
        this.jto = new JdbcTemplate(dataSource);
    }

    @Autowired
    public void setRgService ( ResearchGroupService rgService ) {
        this.rgService = rgService;
    }

    /**
     * Adds a User to the database
     * @param dataModel The User to add to the database
     * @throws com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException
     */
    @Override
    public void addToDb(User dataModel) throws SQLException, MySQLIntegrityConstraintViolationException{

        //
        String query = "INSERT INTO user "
                + "(fname, "
                + "lname, "
                + "email, "
                + "uname)"
                + "VALUES (?, ?, ?, ?);";
        jto.update(
                query,
                new Object[]{
                    dataModel.getFirstName(),
                    dataModel.getLastName(),
                    dataModel.getEmailAddress(),
                    dataModel.getUserName() } );
                    //Close update
    }

    /**
     * Removes a specific user from the database
     * @param <String>
     * @param userid
     * @param userName
     */
    @Override
    public <String> void removeFromDb(String userid) throws DataIntegrityViolationException{

        //
        java.lang.String query = ""
                + "DELETE from belong "
                + "WHERE userid = ?;";
        java.lang.String query1 = ""
                + "DELETE from user "
                + "WHERE id = ?;";

        jto.update(
                query,
                new Object[] {
                    userid
                });

        jto.update(
                query1,
                new Object[] {
                    userid
                });
    }

    /**
     * Get a user model with information extracted from the database
     * @param <String>
     * @param identifier The username of the user to extract from the database
     * @return A User model representing the requisite user
     */
    @Override
    public <String> User getModel(String identifier) {

        //Need to use java.lang to stop type variable issues
        //Can optimize and adjust this later
        java.lang.String query = ""
                + "SELECT "
                + "id, "
                + "fname, "
                + "lname, "
                + "email, "
                + "uname "
                + "FROM user "
                + "WHERE uname =? AND uname != 1";

        User model = jto.queryForObject(
                query,
                new Object[] { identifier },
                new UserRowMapper() );

        return model;
    }

    /**
     * Get a user model with information extracted from the database
     * @param identifier The id of the user to extract from the database
     * @return A User model representing the requisite user
     */
    public User getModel(Integer identifier) {

        //Need to use java.lang to stop type variable issues
        //Can optimize and adjust this later
        java.lang.String query = ""
                + "SELECT "
                + "id, "
                + "fname, "
                + "lname, "
                + "email, "
                + "uname "
                + "FROM user "
                + "WHERE id =?";

        User model = jto.queryForObject(
                query,
                new Object[] { identifier },
                new UserRowMapper() );

        return model;
    }


    /**
     * Gets a list of all of the users in the database
     * TODO Make sure that this works given the non-compliance of the User model with the database table
     * @return List of users in the DB
     */
    @Override
    public List<User> getModels() {

        //TODO define new row mapper that includes the listification of groups

        //Gentleman, we have a winner

        //The changed query should take care of the potential automapping issues
        String query = ""
                + "SELECT "
                + "id, "
                + "fname, "
                + "lname, "
                + "email, "
                + "uname "
                + "FROM user WHERE id != 1;";

        List<User> userModels = jto.query(
                query,
                new RowMapperResultSetExtractor<User>(
                    new UserRowMapper() ) );

        return userModels;
    }

    /**
     * Gets a list of all of the ids users that belong to a specific research group
     * @param groupId The id of the research group that the users should belong to
     * @return <List> of the users
     */
    public List<Integer> getModelsInGroup (int groupId) {

        String query = ""
                + "SELECT "
                + "user.id "
                + "FROM user "
                + "INNER JOIN belong ON user.id=belong.userid\n"
                + "INNER JOIN researchgroup ON belong.researchgroupid=researchgroup.id\n"
                + "WHERE researchgroupid=?;";

        List<Integer> userModels = jto.query(
                query,
                new Object[] { ( (Integer) groupId ).toString() },
                new RowMapperResultSetExtractor<Integer>(
                    new UserIdRowMapper() ) );

        return userModels;
    }

       private class EnhancedUserRowMapper implements RowMapper<EnhancedUser> {

        @Override
        public EnhancedUser mapRow(ResultSet rs, int i) throws SQLException {

            EnhancedUser user = new EnhancedUser(
                    rs.getString( "photourl" ),
                    rs.getString( "description" ),
                    ( ( Integer ) rs.getInt( "staffid" )).toString(), //<<----
                    ( ( Integer ) rs.getInt( "studentid" )).toString(), //<<------
                    rs.getInt( "id" ),
                    rs.getString( "fname" ),
                    rs.getString( "lname" ),
                    rs.getString( "email" ),
                    rs.getString( "uname" ));

            String staff = null;
             String student = null;

            try {
                staff  = jto.queryForObject(
                        "SELECT position FROM staff, user WHERE (staff.id = user.staffid) AND (user.id = "+user.getUserId()+");", String.class);
            } catch ( EmptyResultDataAccessException e ) {
                //
            }

            try {
                student  = jto.queryForObject(
                        "SELECT degree FROM student, user WHERE (student.id = user.studentid) AND (user.id = "+user.getUserId()+");", String.class);
            } catch (EmptyResultDataAccessException e) {
                //
            }

            user.setStaffPos(staff);
            user.setStudentId(student);

            return user;
        }

    }




    /**
     * Gets a list of all of the users that belong to a specific research group
     * with all of their public user information
     * @param groupId The id of the research group that the users should belong to
     * @return <List> of the users
     */
    public List<EnhancedUser> getFullModelsInGroup (Integer groupId) {

        String query = ""
                + "SELECT "
                + "* "
                + "FROM user "
                + "INNER JOIN belong ON user.id=belong.userid\n"
                + "INNER JOIN researchgroup ON belong.researchgroupid=researchgroup.id\n"
                + "WHERE researchgroupid=?;";

        List<EnhancedUser> userModels = jto.query(
                query,
                new Object[] { ( (Integer) groupId ).toString() },
                new RowMapperResultSetExtractor<EnhancedUser>(
                    new EnhancedUserRowMapper() ) );

        return userModels;
    }

    /*  Specialised queries  */

    public boolean addToDb ( User model, String password, int privilege ) {

        try {

            this.addToDb ( model );

            String query = ""
                    + "UPDATE user "
                    + "SET "
                    + "password = ?, "
                    + "privilege = ? "
                    + "WHERE uname = ?;";

            //Required this because it didn't like a method invocation insode update, yet you can have anon classes
            String uname = model.getUserName();

            jto.update(
                    query,
                    new Object[] { password, privilege, uname } );


        } catch (SQLException sqle) {
            //Print our error and return a fail result
            System.out.println(sqle.toString());
            return false;
        }

        return true;
    }

    public void elevateToGroupC ( Object id ) {

        String query = ""
                + "UPDATE user "
                + "SET "
                + "privilege = 7 "
                + "WHERE id = ?";

        jto.update(
                query,
                new Object[] { id } );
    }

    /* User Entity Area */

    public FullUserEntity extractFullUserEntity (String userName) throws SQLException {

        String queryPassword = ""
                + "SELECT password, privilege "
                + "FROM user "
                + "WHERE uname "
                + " = ?;";
        FullUserEntity fue = jto.queryForObject(
                queryPassword,
                new Object[] { userName },
                new FullUserEntityRowMapper() );

        fue.setUserModel( this.getModel( userName ) );
        return fue;
    }

    private class FullUserEntityRowMapper implements RowMapper<FullUserEntity> {

        @Override
        public FullUserEntity mapRow(ResultSet rs, int i) throws SQLException {

            return new FullUserEntity(
                    rs.getString( "password" ),
                    rs.getInt( "privilege" ));
        }
    }

    /* Profile update things */

    public void updateProfileInformation ( ProfileContainer cont, Integer id ) {

        String queryUpdateUserDetails = ""
                + "UPDATE user "
                + "SET "
                + "password = ?, "
                + "description = ? ";

        String colon = " WHERE id = ?;";

        String staffPos;
        String stuDeg;

        int staffId = jto.queryForInt("SELECT staffid FROM user WHERE user.id = " + id + " ;");
        if (staffId == 0) {

            if ( cont.getStaffPosition() != null && ! cont.getStaffPosition().equals("") ) {
                String query = ""
                        + "INSERT INTO staff VALUES (NULL, \" " + cont.getStaffPosition() + " \"); ";
                jto.update(query);
                staffPos = ", staffid =" + jto.queryForInt(" SELECT id FROM staff ORDER BY id DESC LIMIT 1;") + "";
                queryUpdateUserDetails = queryUpdateUserDetails + staffPos;
            }
        } else if ( cont.getStaffPosition() != null && ! cont.getStaffPosition().equals("") ) {
            jto.update("UPDATE staff SET position = ? WHERE id = ?;",
                    new Object[] { cont.getStaffPosition(), staffId } );
        }

        int studentId = jto.queryForInt("SELECT studentid FROM user WHERE user.id = " + id + " ;");
        if (studentId == 0) {
            if (cont.getStudentDegree() != null && ! cont.getStudentDegree().equals("") ) {
                String query = ""
                        + "INSERT INTO student VALUES (NULL, \" " + cont.getStudentDegree() + " \"); ";
                jto.update(query);
                stuDeg = ", studentid = " + jto.queryForInt(" SELECT id FROM student ORDER BY id DESC LIMIT 1; ") + "";
                queryUpdateUserDetails += stuDeg;
            }
        } else if ( cont.getStudentDegree() != null && ! cont.getStudentDegree().equals("") ) {
             jto.update("UPDATE student SET degree = ? WHERE id = ?;",
                    new Object[] { cont.getStudentDegree(), studentId } );
        }

        queryUpdateUserDetails += colon;

        jto.update(
                queryUpdateUserDetails,
                new Object[] {
                    cont.getPassword(),
                    cont.getDescription(),
                    id } );

    }

    public void updatePhotoUrl ( Integer id, String url ) {

        String query = ""
                + "UPDATE user SET photourl = ? "
                + "WHERE id = ?";

        jto.update(
                query,
                new Object[] { url, id });
    }


}
