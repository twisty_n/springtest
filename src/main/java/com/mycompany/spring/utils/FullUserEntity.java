//TODO add header
package com.mycompany.spring.utils;

import com.mycompany.spring.models.User;

/**
 *  Encapsulates the user model as well as their credentials
 * @author tristan
 */
public class FullUserEntity {

    private String password;
    private Integer permissionLevel;

    private User userModel;

    public FullUserEntity() {
        this.password = null;
        this.permissionLevel = null;
        this.userModel = null;
    }

    public FullUserEntity(String password, int permissionLevel, User userModel) {
        this.password = password;
        this.permissionLevel = permissionLevel;
        this.userModel = userModel;
    }

    public FullUserEntity(String password, int permissionLevel) {
        this.password = password;
        this.permissionLevel = permissionLevel;
        this.userModel = null;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getPermissionLevel() {
        return permissionLevel;
    }

    public void setPermissionLevel(Integer permissionLevel) {
        this.permissionLevel = permissionLevel;
    }

    public User getUserModel() {
        return userModel;
    }

    public void setUserModel(User userModel) {
        this.userModel = userModel;
    }


}
