/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.spring.service;

import com.mycompany.spring.controller.MeFiPoController;
import com.mycompany.spring.dao.ThreadDocDAO;
import com.mycompany.spring.models.AccessRecord;
import com.mycompany.spring.models.FiPoThread;
import com.mycompany.spring.models.MePoThread;
import com.mycompany.spring.models.Post;
import com.mycompany.spring.models.User;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

/**
 *  Service layer to provide the functionality for Thread/Document/Meeting ops
 * @author tristan
 */
@Service
public class ThreadDocService {

    private ThreadDocDAO threadDocDAO;
    private JavaMailSender jms;

    @Autowired
    public void setThreadDocDataAccessObjec( ThreadDocDAO tddao ) {
        this.threadDocDAO = tddao;
    }

    @Autowired
    public void setJms(JavaMailSender jms) {
        this.jms = jms;
    }

    /**
     * Adds a meeting to the database and emails the users as required
     * @param mpt MePoThread Model containing the information to add
     * @param emailList List of users of the research group to email
     */
    public void createMeeting(MePoThread mpt, List<User> emailList) {
        this.threadDocDAO.createMeeting(mpt);
        this.sendNotificationEmail(emailList, mpt);
    }

    /**
     * Returns a list of meetings belonging to some group
     * @param groupId
     * @return
     */
    public List<MePoThread> viewMeetingList ( Integer groupId ) {
        return this.threadDocDAO.retrieveMeetingList( groupId );
    }

    /**
     * Creates a post as relating to a specific thread
     * @param post The model containing the post information
     */
    public void createPost(Post post) {
        //Look here for post related bugs   ------------\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
        this.threadDocDAO.createPost( post, post.getThreadId() );
    }

    /**
     * Returns a list of file threads as pertaining to some group
     * @param groupId
     * @return
     */
    public List<FiPoThread> viewFileThreadList ( Integer groupId ) {
        return  this.threadDocDAO.retrieveFileThreadList( groupId );
    }

    /**
     * Returns a list of discussion threads as pertaining to some group
     * @param groupId
     * @return
     */
    public List<Post> viewDiscussionThreadList ( Integer groupId ) {
        return this.threadDocDAO.retriveDiscussionThreadList( groupId );
    }

    /**
     * Retrieves all the posts in a given thread and creates an access log entry
     * @param threadId
     * @return
     */
    public List<Post> viewPosts ( Integer threadId, User thisUser ) {
        //
        this.threadDocDAO.createAccessRecord( threadId, thisUser.getUserId() );
        return this.threadDocDAO.retrievePostsInThread( threadId );
    }

    /**
     * Retrieves the thread details for a particular thread
     * @param threadId
     * @return
     */
    public Post retrieveThreadDetails ( Integer threadId ) {
        return this.threadDocDAO.retrieveThreadDetails ( threadId );
    }

    /**
     * Creates a new thread on the discussion board
     * @param p
     */
    public void createThread( Post p ) {
        this.threadDocDAO.createDiscussionThread(p);
    }

    /**
     * Creates a new file record and returns the Id of this as a unique identifier
     * @param fps
     * @param emailList
     * @return
     */
    public Integer createAndRetrieveFileRecord( FiPoThread fps, List<User> emailList ) {
        this.sendNotificationEmail( emailList, fps );
        return this.threadDocDAO.createFileRecord( fps );
    }

    public List<AccessRecord> retrieveAccessRecords ( Integer groupId ) {
        return this.threadDocDAO.retrieveAccessRecords( groupId );
    }

    /**
     * WARNING requires a valid email list
     * @param emailList
     * @param p
     */
    private void sendNotificationEmail (List<User> emailList, Post p ) {

        SimpleMailMessage email = new SimpleMailMessage();
        String[] userList = new String[emailList.size()]; int i = 0;
        StringBuilder message = new StringBuilder("Research Group Notification");
        message.append("\n");
        message.append("There has been activity in Thread: ").append(p.getName());
        message.append("\nNew content: ").append(p.getContent());
        for (User user : emailList) {
            userList[i++] = user.getEmailAddress();
        }
        email.setTo(userList);
        email.setSubject("Research Group Notification");
        email.setText(message.toString());

        try {
            this.jms.send(email);
        } catch ( NullPointerException ex ) {
            Logger.getLogger(MeFiPoController.class.getName()).log(
                    Level.SEVERE,
                    "Sending failed. Malformed address", ex);
        }

    }

}
