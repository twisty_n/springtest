//TODO add decent header
package com.mycompany.spring.service;

import com.mycompany.spring.dao.ResearchGroupDAO;
import com.mycompany.spring.models.ResearchGroup;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Service layer for the Research Group Operations
 * @author tristan
 */
@Service
public class ResearchGroupService {

    private ResearchGroupDAO researchDAO;

    @Autowired
    public void setResearchDAO ( ResearchGroupDAO rDAO ) {
        this.researchDAO = rDAO;
    }

    /**
     * Adds a research group to the database
     * @param rGroup The group to add
     */
    public void addResearchGroupToSystem ( ResearchGroup rGroup ) {
        this.researchDAO.addToDb( rGroup );
    }

    /**
     * Removes a research group from the database
     * @param rGroup The group to be removed
     */
    public void removeResearchGroupFromSystem ( Integer id ) {
        this.researchDAO.removeFromDb( id );
    }

    /**
     * Retreives a research groups information from the database
     * @param groupId The ID of the group to be retreived
     * @return ResearchGroup The RG specified
     */
    public ResearchGroup retreiveResearchGroup ( Integer groupId ) {
        return this.researchDAO.getModel( groupId );
    }

    /**
     * Retrives a list of all of the research groups in the database
     * @Warning :: Use cautiously
     * @return A <List> of all of the research groups
     */
    public List<ResearchGroup> retreiveAllResearchGroups () {
        return this.researchDAO.getModels();
    }

    /**
     * Retrieves a list of all of the ids of the research groups that the user belongs to
     * @param userId The id of the user that the groups should be mapped to
     * @return The <List> of groups
     */
    public List<Integer> retreiveGroupsMappedToUser ( int userId ) {
        return this.researchDAO.getGroupsInUser( userId );
    }

    /**'
     * Assigns a new coordinator to a research group
     * @param groupId
     * @param userId
     */
    public void assignNewGroupCoordinator( Integer groupId, Integer userId ) {
        this.researchDAO.assignNewCoordinator(groupId, userId);
    }

    /**
     * Returns a list of all of the research groups administered by a specfic coordinator
     * @param id
     * @return
     */
    public List<ResearchGroup> getGroupsAccordingUnderCoordinator ( Object id ) {
        return this.researchDAO.getModels( id );
    }

    /**
     * Updates a research groups informatoin to the supplied
     * @param name
     * @param description
     * @param id
     */
    public void updateGroupDetails( String name, String description, Integer id ) {
        this.researchDAO.updateGroupInformation(name, description, id);
    }

    /**
     * Adds a new person to the research group
     * @param userId
     * @param groupId
     */
    public void addNewUserToGroup (Integer userId, Integer groupId) {
        this.researchDAO.createGroupPersonMapping(userId, groupId);
    }

    /**
     * Returns a list of full RG objects that the user is a part of
     * @param userId
     * @return
     */
    public List<ResearchGroup> getGroupsThatUserBelongsTo( Integer userId ) {
        return this.researchDAO.getGroupsInUser(userId);
    }

    /**
     * Removes the specified person from the specified group
     * @param userId
     * @param groupId
     */
    public void removePersonFromResearchGroup( Integer userId, Integer groupId ) {
        this.researchDAO.removePersonFromGroup(userId, groupId);
    }

}
