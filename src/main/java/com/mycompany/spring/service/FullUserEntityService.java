/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.spring.service;

import com.mycompany.spring.dao.UserDAO;
import com.mycompany.spring.utils.FullUserEntity;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 *  Services the validation and session subscription for a user entity
 * @author tristan
 */
@Service
public class FullUserEntityService implements UserDetailsService{

    private UserDAO fueDAO;

    private HttpSession session;

    @Autowired
    public void setSession(HttpSession session) {
        this.session = session;
    }

    @Autowired
    public void setFueDAO (UserDAO fueDAO) {
        this.fueDAO = fueDAO;
    }

    @Override
    @SuppressWarnings("unchecked")
    public UserDetails loadUserByUsername ( String loginUsername ) throws UsernameNotFoundException {
        try {

            FullUserEntity user = this.fueDAO.extractFullUserEntity(loginUsername);

            boolean enabled = true;
            boolean accountNotExpired = true;
            boolean credentialsNotExpired = true;
            boolean accountNotLocked = true;

            //this.session.setAttribute( "userModel" , user.getUserModel() );

            return new User(
                    user.getUserModel().getUserName(),
                    user.getPassword(),
                    enabled,
                    accountNotExpired,
                    credentialsNotExpired,
                    accountNotLocked,
                    getAuthorities( user.getPermissionLevel() ) );

        } catch (SQLException ex) {
            Logger.getLogger(FullUserEntityService.class.getName()).log(Level.SEVERE, null, ex);
            throw new UsernameNotFoundException("Credentials invalid. User not found");
        }
    }

    public List getAuthorities( Integer role ) {
        List<GrantedAuthority> authList = getGrantedAuthorities( getRoles( role ) );
        return authList;
    }

    @SuppressWarnings("fallthrough")
    public List<String> getRoles( Integer role ) {

        List<String> roles = new ArrayList<String>();

        switch ( role ) {

            case 9 : {
                roles.add( "ROLE_ADMIN" );
                break;
            }

            case 7 : {
                roles.add( "ROLE_GROUP_COORDINATOR" );
            }

            case 5 : {
                roles.add( "ROLE_STAFF" );
                roles.add( "ROLE_USER" );
                break;
            }

            case 3 : {
                roles.add( "ROLE_STUDENT" );
                roles.add( "ROLE_USER" );
                break;
            }

            case 1 : {
                roles.add( "ROLE_VISITOR" );
            }

            default : {
                roles.add( "ROLE_USER" );
                break;
            }

        }
        return roles;
    }

    public static List<GrantedAuthority> getGrantedAuthorities (List<String> roles) {

        List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();

        for (String role : roles) {
            authorities.add(new SimpleGrantedAuthority( role ) );
        }

        return authorities;
    }

}
