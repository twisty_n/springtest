//TODO Add doc header
package com.mycompany.spring.service;

import com.mycompany.spring.dao.UserDAO;
import com.mycompany.spring.models.EnhancedUser;
import com.mycompany.spring.models.ProfileContainer;
import com.mycompany.spring.models.User;
import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;
import java.sql.SQLException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;

/**
 * Service layer separating the DB logic from the controller
 * Use it as a pass through and to perform other tasks as part of any query
 * service or routine e.g. As the basis for a notification layer
 * @author tristan
 */
@Service
public class UserService {

    private UserDAO userDataAccessObject;

    @Autowired
    public void setUserDataAccessObject ( UserDAO udao ) {
        this.userDataAccessObject = udao;
    }

    /**
     * Adds a user to the database
     * @param user The User to add
     * @throws java.sql.SQLException
     * @throws com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException
     */
    public void addUserToSystem( User user ) throws SQLException, MySQLIntegrityConstraintViolationException{
        this.userDataAccessObject.addToDb( user );
    }

    /**
     * Deletes a user from the database
     * @param userid
     */
    public void removeUserFromSystem( String userid ) throws DataIntegrityViolationException{
        this.userDataAccessObject.removeFromDb( userid );
    }

    /**
     * Retrieves a users information from the database
     * @param userName Users uname to extract with
     * @return The User model
     */
    public User retrieveUser ( String userName ) {
        return this.userDataAccessObject.getModel( userName );
    }

    /**
     * Retrieves a list of all of the users in the database
     * @Warning :: Use wisely
     * @return A <List> of all of the users in the database
     */
    public List<User> retrieveAllUsers() {
        return  this.userDataAccessObject.getModels();
    }

    /**
     * Returns a list of all of the users that belong to a specific group
     * @param groupId
     * @return
     */
    public List<Integer> getUsersInGroup ( int groupId ) {
        return this.userDataAccessObject.getModelsInGroup( groupId );
    }

    /**
     * Returns a List of User models that belong to a specific group
     * @param id
     * @return
     */
    public List<EnhancedUser> getAllEnhancedUsersInGroup ( Integer id ) {
        return this.userDataAccessObject.getFullModelsInGroup ( id );
    }

    /**
     * Adds a new user to the database
     * @param model
     * @param password
     * @param priv
     * @return
     */
    public boolean addUserToDatabase ( User model, String password, int priv ) throws DuplicateKeyException {
        return this.userDataAccessObject.addToDb(model, password, priv);
    }

    /**
     * Elevates the specified user to the Group Coordinator role
     * @param id
     */
    public void elevateToGC ( Object id ) {
        this.userDataAccessObject.elevateToGroupC( id );
    }

    /**
     * Updates a user profile with the provided information
     * @param cont
     * @param id
     */
    public void updateUserProfile (ProfileContainer cont, Integer id) {
        this.userDataAccessObject.updateProfileInformation(cont, id);
    }

    /**
     * Updates a users photo ID to the one provided
     * @param id
     * @param url
     */
    public void updatePhotoUrl ( Integer id, String url ) {
        this.userDataAccessObject.updatePhotoUrl(id, url);
    }

    /**
     * Gets a user Model from the database represented by Id
     * @param id
     * @return The user model
     */
    public User getUserById( Integer id ) {
        return this.userDataAccessObject.getModel( id );
    }

}
