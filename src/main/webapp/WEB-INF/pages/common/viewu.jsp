<%--
    Document   : viewu
    Created on : 08/11/2013, 10:35:39 PM
    Author     : tristan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>
            <c:out value="${ userToView.getFirstName() }"></c:out>
            <c:out value="${ userToView.getLastName() }"></c:out>
            </title>
        </head>
        <body>
            <table>
                <tr>
                    <td>
                        <a href="${pageContext.request.contextPath}/landing.html">Landing Page</a>
                </td>
                <td>
                    <a href="${pageContext.request.contextPath}/common/viewg?g=${ g.getGroupId() }">Group Area</a>
                </td>
                <td>
                    <a href="<c:url value="/j_spring_security_logout"></c:url>">Logout</a>
                    </td>
                </tr>
            </table>
            <h1>
                You are viewing <c:out value="${ userToView.getFirstName() }"></c:out>
            <c:out value="${ userToView.getLastName() }"></c:out>'s Profile
            </h1>
            <table>
                <tr>
                    <td>
                        First Name:
                    </td>
                    <td>
                    <c:out value="${ userToView.getFirstName() }"></c:out>
                    </td>
                </tr>
                <tr>
                    <td>
                        Last Name:
                    </td>
                    <td>
                    <c:out value="${ userToView.getLastName() }"></c:out>
                    </td>
                </tr>
                <tr>
                    <td>
                        Email Address:
                    </td>
                    <td>
                    <c:out value="${ userToView.getEmailAddress() }"></c:out>
                    </td>
                </tr>
                <tr>
                    <td>
                        Username:
                    </td>
                    <td>
                    <c:out value="${ userToView.getUserName() }"></c:out>
                    </td>
                </tr>
                <tr>
                    <td>
                        Staff Position:
                    </td>
                    <td>
                    <c:out default="N/A" value="${ userToView.getStaffPos() }"></c:out>
                    </td>
                </tr>
                <tr>
                    <td>
                        Student Degree:
                    </td>
                    <td>
                    <c:out default="N/A" value="${ userToView.getStudentId() }"></c:out>
                    </td>
                </tr>
                <tr>
                    <td>
                        Bio:
                    </td>
                    <td>
                    <c:out default="N/A" value="${ userToView.getDescription() }" ></c:out>
                    </td>
                </tr>
            </table>
            <img style="max-height: 150px;  max-width: 120px" src="${ pageContext.request.contextPath }/resources/${ userToView.getPhotoUrl() }"></img><br />
    </body>
</html>
