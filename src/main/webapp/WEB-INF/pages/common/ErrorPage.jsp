<%--
    Document   : ErrorPage
    Created on : 10/11/2013, 10:10:44 PM
    Author     : tristan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Uh Oh and Error has occured</title>
    </head>
    <body>
        <img src="${ pageContext.request.contextPath }/resources/error.png"></img><br />
        <a href="<c:url value="/j_spring_security_logout"></c:url>">Logout</a>
    </body>
</html>
