<%--
    Document   : viewg
    Created on : 08/11/2013, 8:07:20 PM
    Author     : tristan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><c:out value="${ group.getGroupName() }"></c:out></title>
            <style>
                .display{
                    border-collapse: collapse;
                    border: 1px solid black;
                    border-width: 3px;
                    word-wrap: break-word;

                }
                .display td{
                    border: 1px solid black;
                    padding-left: 7px;
                    padding-right: 7px;
                    max-width: 300px;
                }
            </style>
        </head>
        <body>
            <table>
                <tr>
                    <td>
                        <a href="${pageContext.request.contextPath}/landing.html">Landing Page</a>
                </td>
                <td>
                    <a href="<c:url value="/j_spring_security_logout"></c:url>">Logout</a>
                    </td>
                </tr>
            </table>
            <h1>
                You are viewing the <c:out value="${ group.getGroupName() }"></c:out> Group Page
            </h1>
            <h2>
                Group Information
            </h2>
            <table>
                <tr>
                    <td>
                        Group Name:
                    </td>
                    <td>
                    <c:out value="${ group.getGroupName() }"></c:out>
                    </td>
                </tr>
                <tr>
                    <td>
                        Group Description:
                    </td>
                    <td>
                    <c:out value="${ group.getGroupDescription() }"></c:out>
                    </td>
                </tr>
                <tr>
                    <td>
                        Group Coordinator:
                    </td>
                    <td>
                    <c:forEach items="${ userList }" var="u">
                        <c:if test="${u.getUserId() == group.getGroupCoordinator() }">
                            <c:out value="${ u.getFirstName() }"></c:out> <c:out value="${ u.getLastName() }"></c:out>
                        </c:if>
                    </c:forEach>
                </td>
            </tr>
            <tr>
                <td>
                    Group Members:
                </td>
                <td>
                    <c:forEach items="${ userList }" var="u">
                        <a href="/common/viewp?id=${u.getUserId()}">
                            <c:out value="${ u.getFirstName() }"></c:out> <c:out value="${ u.getLastName() }"></c:out>
                            </a> <br />
                    </c:forEach>
                </td>
            </tr>
        </table>

        <h4>Meetings</h4>
        <table class="display">
            <thead>
                <tr>
                    <th>
                        Name
                    </th>
                    <th>
                        Date
                    </th>
                    <th>
                        Location
                    </th>
                    <th></th>
                </tr>
            </thead>
            <c:forEach items="${ meetings }" var="m">
                <tr>
                    <td>
                        <c:out value="${ m.getName() }"></c:out>
                        </td>
                        <td>
                        <c:out value="${ m.getDateAndTime() }"></c:out>
                        </td>
                        <td>
                        <c:out value="${ m.getLocation() }"></c:out>
                        </td>
                        <td>
                            <a href="/common/view-thread?thread=${ m.getThreadId() }"> View! </a>
                    </td>
                </tr>
            </c:forEach>
        </table>

        <h4>Documents</h4>
        <a href="/common/upload-doc">Upload Document</a><br/>
        <em style="color: red">
            <c:out default="" value="${ docerror }"></c:out>
            </em>
            <table class="display">
                <thead>
                    <tr>
                        <th>
                            File name
                        </th>
                        <th>
                            Version
                        </th>
                        <th>
                            Format
                        </th>
                        <th>
                            Uploader
                        </th>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>
            <c:forEach varStatus="index"  items="${ files }" var="f">
                <tr>
                    <td>
                        <c:out value="${ f.getName() }"></c:out>
                        </td>
                        <td>
                        <c:out value="${ f.getVersion() }"></c:out>
                        </td>
                        <td>
                        <c:out value="${ f.getFormat() }"></c:out>
                        </td>
                        <td>
                        <c:out value="${ f.getThreadCreator().getFirstName() }"></c:out>
                        <c:out value="${ f.getThreadCreator().getLastName() }"></c:out>
                        </td>
                        <td>
                            <a href="/common/view-thread?thread=${ f.getThreadId() }"> View! </a>
                    </td>
                    <td>
                        <a href="/common/download?ref=${ index.index }"> Download! </a>
                    </td>
                </tr>
            </c:forEach>
        </table>

        <h4>Discussion Board</h4>
        <a href="/common/create-thread">Create new Thread!</a><br/>
        <table class="display">
            <thead>
                <tr>
                    <th>
                        Thread Name
                    </th>
                    <th>
                        Date Created
                    </th>
                    <th>
                        Created by
                    </th>
                    <th></th>
                </tr>
            </thead>
            <c:forEach items="${ threads }" var="t">
                <tr>
                    <td>
                        <c:out value="${ t.getName() }"></c:out>
                        </td>
                        <td>
                        <c:out value="${ t.getDateCreated() }"></c:out>
                        </td>
                        <td>
                        <c:out value="${ t.getThreadCreator().getFirstName() }"></c:out>
                        <c:out value="${ t.getThreadCreator().getLastName() }"></c:out>
                        </td>
                        <td>
                            <a href="/common/view-thread?thread=${ t.getThreadId() }"> View! </a>
                    </td>
                </tr>
            </c:forEach>
        </table>

    </body>
</html>
