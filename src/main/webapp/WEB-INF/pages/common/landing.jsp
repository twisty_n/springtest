<%--
    Document   : landing
    Created on : 07/11/2013, 6:51:14 PM
    Author     : tristan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Landing Swoosh, its a Jet!</title>
        <style>
            nav ul {
	background: #efefef;
	background: linear-gradient(top, #efefef 0%, #bbbbbb 100%);
	background: -moz-linear-gradient(top, #efefef 0%, #bbbbbb 100%);
	background: -webkit-linear-gradient(top, #efefef 0%,#bbbbbb 100%);
	box-shadow: 0px 0px 9px rgba(0,0,0,0.15);
	padding: 0 20px;
	border-radius: 10px;
	list-style: none;
	position: relative;
	display: inline-table;
}
	nav ul:after {
		content: ""; clear: both; display: block;
	}

        nav ul li {
	float: left;
}
	nav ul li:hover {
		background: #4b545f;
		background: linear-gradient(top, #4f5964 0%, #5f6975 40%);
		background: -moz-linear-gradient(top, #4f5964 0%, #5f6975 40%);
		background: -webkit-linear-gradient(top, #4f5964 0%,#5f6975 40%);
	}
		nav ul li:hover a {
			color: darkorange;
		}

	nav ul li a {
		display: block; padding: 15px 20px;
		color: #757575; text-decoration: none;
	}
        nav ul ul {
	display: none;
}

	nav ul li:hover > ul {
		display: block;
	}
        </style>
    </head>
    <body>
        <a href="<c:url value="/j_spring_security_logout"></c:url>">Logout</a>
        <h1>Hello <c:out value="${ thisUser.getFirstName() }"></c:out> <c:out value="${ thisUser.getLastName() }"></c:out></h1>

        Your groups: <br />
        <nav>
            <ul>
            <c:forEach  items="${ groupsBelongedTo }" var="z">
                <li>
                    <a href="/common/viewg?g=${z.getGroupId()}"><c:out value="${ z.getGroupName() }"></c:out></a>
                </li>
            </c:forEach>
        </ul>
        </nav>

        <div class="nav">
                <a href="<c:url value="/common/edit-profile"></c:url>">Edit Profile</a><br />
        </div>

            <div>
            <security:authorize access="hasRole('ROLE_GROUP_COORDINATOR')">
                <br />
                <!-- This is for the Group Coordinators to be editing group stuff -->
                <form method="post" action='${pageContext.request.contextPath}/coord/editGroup'>
                    <label>
                        Edit Group: <br />
                        <select name="group">
                            <c:forEach  items="${ groupList }" var="u">
                                <option  value="${ u.getGroupId() }">
                                    <c:out value="${ u.getGroupName() }"></c:out>
                                    </option>
                            </c:forEach>
                        </select>
                    </label>
                    <input type="Submit" name="submit" value='Edit!' />
                </form>
            </security:authorize>
        </div>

    </body>
</html>
