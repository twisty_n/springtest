<%--
    Document   : view-thread
    Created on : 10/11/2013, 12:54:11 PM
    Author     : tristan
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>
            <c:out value="${ threadDetails.getName() }"></c:out>
            </title>
            <style>
                .display{
                    border-collapse: collapse;
                    border: 1px solid black;
                    border-width: 3px;
                    word-wrap: break-word;

                }
                .display td{
                    border: 1px solid black;
                    padding-left: 7px;
                    padding-right: 7px;
                    max-width: 300px;
                }
            </style>
        </head>
        <body>
            <table>
                <tr>
                    <td>
                        <a href="${pageContext.request.contextPath}/landing.html">Landing Page</a>
                </td>
                <td>
                    <a href="${pageContext.request.contextPath}/common/viewg?g=${ g.getGroupId() }">Group Area</a>
                </td>
                <td>
                    <a href="<c:url value="/j_spring_security_logout"></c:url>">Logout</a>
                    </td>
                </tr>
            </table>
            <h1>You are viewing <c:out value="${ threadDetails.getName() }"></c:out></h1>
            <div>
                Thread created by: <c:out value="${ threadDetails.getThreadCreator().getFirstName() }"></c:out>
            <c:out value="${ threadDetails.getThreadCreator().getLastName() }"></c:out><br/>
            Created: <c:out value="${ threadDetails.getDateCreated() }"></c:out>
            </div><br>
            <div>
                <table class="display">
                    <thead>
                        <tr>
                            <th>
                                Post
                            </th>
                            <th>
                                Posted By
                            </th>
                            <th>
                                Date Posted
                            </th>
                        </tr>
                    </thead>
                <c:forEach items="${ posts }" var="p">
                    <tr>
                        <td>
                            <c:out value="${ p.getContent() }"></c:out>
                            </td>
                            <td>
                            <c:out value="${ p.getPoster().getFirstName() }"></c:out>
                            <c:out value="${ p.getPoster().getLastName() }"></c:out>
                            </td>
                            <td>
                            <c:out value="${ p.getDatePosted() }"></c:out>
                            </td>
                        </tr>
                </c:forEach>
            </table>
        </div>
        <div>
            <br/>
            <br/>
            <form:form method="post" commandName="post" action="${pageContext.request.contextPath}/common/doPost">
                <label>
                    Make a post: <br/>
                </label>
                <form:textarea required="true" rows="6" cols="50" path="content"></form:textarea><br/>
                    <input type="submit" value="Post!" />
            </form:form>
        </div>
    </body>
</html>
