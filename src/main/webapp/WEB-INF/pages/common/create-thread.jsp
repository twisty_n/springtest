<%--
    Document   : create-thread
    Created on : 10/11/2013, 3:04:23 PM
    Author     : tristan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Create new thread</title>
    </head>
    <body>
        <table>
            <tr>
                <td>
                    <a href="${pageContext.request.contextPath}/landing.html">Landing Page</a>
                </td>
                <td>
                    <a href="${pageContext.request.contextPath}/common/viewg?g=${ g.getGroupId() }">Group Area</a>
                </td>
                <td>
                    <a href="<c:url value="/j_spring_security_logout"></c:url>">Logout</a>
                    </td>
                </tr>
            </table>
            <h1>Create a new discussion thread</h1>
        <form:form commandName="threadPost"
                   method="post" action="${pageContext.request.contextPath}/common/createNewThread">
            <label>
                Thread Name:<br />
                <form:input type="text" required="true" path="name" maxlength="50"></form:input><br/>
                </label>
                <label>
                    Post:<br />
                <form:textarea rows="6" cols="50" required="true" path="content" maxlength="10000"></form:textarea><br/>
                </label>
                <input type="Submit" name="submit" value='Create Thread!' />
        </form:form>
    </body>
</html>
