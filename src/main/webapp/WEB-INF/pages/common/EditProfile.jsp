<%--
    Document   : EditProfile
    Created on : Nov 8, 2013, 2:03:16 AM
    Author     : Tristan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Edit Profile</title>
    </head>
    <body>
        <table>
    <tr>
        <td>
            <a href="${pageContext.request.contextPath}/landing.html">Landing Page</a>
        </td>
        <td>
            <a href="<c:url value="/j_spring_security_logout"></c:url>">Logout</a>
        </td>
    </tr>
</table>
        <h1>Hello <c:out value="${ thisUser.getFirstName() }"></c:out></h1>
        <p>
            Update your profile information here:
        </p>
        <div>
            First Name: <c:out value="${ thisUser.getFirstName() }"></c:out> <br />
            Last Name: <c:out value="${ thisUser.getLastName() }"></c:out> <br />
            Email Address: <c:out value="${ thisUser.getEmailAddress() }"></c:out> <br />
            Username: <c:out value="${ thisUser.getUserName() }"></c:out> <br />
        </div>
        <div>
        <form:form method="post" commandName="profile" action="${pageContext.request.contextPath}/common/doEditProfile">
            <label>Password:
                <form:input required="true" type="text" path="password" name="password" maxlength="30"/>
                <br />
                <form:errors path="password"></form:errors>
                </label><br />

                <label>Bio:<br />
                <form:textarea required="true" rows="5" cols="50" path="description"></form:textarea>
                <form:errors required="true" path="description"></form:errors>
                </label><br />

                <security:authorize access="hasRole('ROLE_STAFF')">
                    <label>
                        Position: <br />
                        <form:input path="staffPosition" name="staffPosition" maxlength="30"/>
                    </label>
                </security:authorize>

                <security:authorize access="hasRole('ROLE_STUDENT')">
                    <label>
                        Degree: <br />
                        <form:input type="text" path="studentDegree" name="studentDegree" maxlength="30" />
                    </label>
                </security:authorize>

                <label><input type="Submit" name="submit" value='Edit!' />
            </label><br />
        </form:form>
            <p>
            <h2>
                Upload a new profile picture
            </h2>
            </p>
        <form:form method="post" commandName="photo"
                   enctype="multipart/form-data" action="${pageContext.request.contextPath}/upload/doUploadPhoto">
            <form:label path="photoUrl"><br />
                <form:input type="file" path="file" required="true" />
                <form:errors path="*" style="color: red"></form:errors>
            </form:label><br />
            <input type="submit" value="Upload!" />
        </form:form>
        </div>
    </body>

