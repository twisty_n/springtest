<%--
    Document   : bad-access
    Created on : 08/11/2013, 8:37:33 PM
    Author     : tristan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Uh Uh Uh</title>
    </head>
    <body>
        <a href="<c:url value="/j_spring_security_logout"></c:url>">Logout</a>
        <h1>Uh Uh Uh - You didn't say the magic word</h1>
        <img src="${ pageContext.request.contextPath }/resources/nedry.gif"></img><br />
    </body>
</html>
