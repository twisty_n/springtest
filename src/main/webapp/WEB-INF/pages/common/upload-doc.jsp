<%--
    Document   : upload-doc
    Created on : 10/11/2013, 3:28:02 PM
    Author     : tristan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Upload a document</title>
    </head>
    <body>
        <table>
            <tr>
                <td>
                    <a href="${pageContext.request.contextPath}/landing.html">Landing Page</a>
                </td>
                <td>
                    <a href="${pageContext.request.contextPath}/common/viewg?g=${ g.getGroupId() }">Group Area</a>
                </td>
                <td>
                    <a href="<c:url value="/j_spring_security_logout"></c:url>">Logout</a>
                    </td>
                </tr>
            </table>
            <h1>Upload a document</h1>
        <form:form method="post" commandName="documentContainer"
                   enctype="multipart/form-data" action="${pageContext.request.contextPath}/upload/doUploadDoc">

            <label>
                Document Name:<br /> <!-- this becomes the thread name -->
                <form:input type="text" required="true" path="name" maxlength="50"></form:input><br/>
                </label>
                <label>
                    Description:<br/> <!-- this is the first post in the thread -->
                <form:textarea rows="5" cols="45" required="true" path="content" maxlength="10000"></form:textarea><br/>
                </label>

                <label>
                    Format (Please ensure this is selected and correct):
                <form:select path="format" required="true">
                    <form:option value="pdf">
                        .pdf
                    </form:option>
                    <form:option value="odf">
                        .odf
                    </form:option>
                    <form:option value="doc">
                        .doc
                    </form:option>
                    <form:option value="doc">
                        .docx
                    </form:option>
                    <form:option value="txt">
                        .txt
                    </form:option>
                </form:select><br/>
            </label>
            Version:
            <form:input path="version" type="number" min="1" max="25"></form:input><br/>

            <form:input type="file" path="file" required="true" /><br/>

            <form:errors path="*" style="color: red"></form:errors>
                <br />
                <input type="submit" value="Upload Document!" />
        </form:form>
        <p style="color: red">
            <c:out default="" value="${ error }" ></c:out>
        </p>
    </body>
</html>
