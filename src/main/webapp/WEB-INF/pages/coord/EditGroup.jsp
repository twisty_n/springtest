<%--
    Document   : EditGroup
    Created on : 07/11/2013, 9:26:48 PM
    Author     : tristan
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Edit <c:out value="${ group.getGroupName() }"></c:out></title>
        <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css">
        <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
        <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
    </head>
</head>
<body>
    <table>
            <tr>
                <td>
                    <a href="${pageContext.request.contextPath}/landing.html">Landing Page</a>
                </td>

                <td>
                    <a href="<c:url value="/j_spring_security_logout"></c:url>">Logout</a>
                    </td>
                </tr>
            </table>
    <h1>
        Edit Group: <c:out value="${ group.getGroupName() }"></c:out>
    </h1>

    <h3 style="color: aqua">
        <c:out  default="" value="${ successYay.toString() }"></c:out><br/>
    </h3>

    <form method="post" action="${pageContext.request.contextPath}/coord/submitEdit.html">
            <label>
                Group Name: <br />
                <input type="text" required name="groupName" value="${ group.getGroupName() }"></input>
            </label>
            <br />
            <label>
                Description
            </label> <br />
            <textarea rows="5" cols="50" name="description" required placeholder="${ group.getGroupDescription() }"></textarea>
            <br />
            <input type="Submit" name="submit" value='Edit' />
        </form>

            <p>
                <br />
            </p>

            <form method="post" action="${pageContext.request.contextPath}/coord/submitUser.html">
                <label>
                    Add a user to the group
                </label>
                <select name="user">
                    <c:forEach items="${userList}" var="u" >
                        <option value="${ u.getUserId() }"  >
                            ${ u.getFirstName() } ${ u.getLastName() }
                        </option>
                    </c:forEach>
                </select>

                <input type="Submit" name="submit" value='Add!' />
            </form>

            <form method="post" action="${pageContext.request.contextPath}/coord/delete-from-group.html">
                <label>
                    Delete a user from the group
                </label>
                <select name="uid">
                    <c:forEach items="${inGroup}" var="u" >
                        <option value="${ u.getUserId() }"  >
                            ${ u.getFirstName() } ${ u.getLastName() }
                        </option>
                    </c:forEach>
                </select>

                <input type="Submit" name="submit" value='Erase!' />
            </form>

                <div><br/>
                    <button href="http://www.textfixer.com"
                       onclick="javascript:void
                           window.open('${pageContext.request.contextPath}/coord/view-access.html', '_blank','width=700,\n\
                            height=500,toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1,left=0,top=0');
                           return false;">View Access Log</button>
                </div>

                <br/>
                <p>
                    Create a group meeting
                </p>
            <!-- TODO: ADD some meeting interface crap in here  -->
            <form:form commandName="MePoThread"
                       method="post" action="${pageContext.request.contextPath}/coord/doCreateMeeting">
                <label>
                    Meeting Name:<br />
                    <form:input type="text" required="true" path="name" maxlength="50"></form:input><br/>
                </label>
                <label>
                    Agenda:<br />
                <form:textarea rows="5" cols="45"  required="true" path="agenda" maxlength="2000"></form:textarea><br/>
                </label>
                <label>
                    Location:<br/>
                    <form:input required="true" path="location" maxlength="100"></form:input><br/>
                </label>
                <label>
                    Description:<br/>
                    <form:textarea rows="5" cols="45" required="true" path="content" maxlength="10000"></form:textarea><br/>
                </label>
                <label>
                    Choose Date and Time:<br/>
                    <form:input type="datetime-local" required="true" path="dateAndTime" id="datetime"></form:input>
                    <label style="color: red">
                        <c:out  default="" value="${ dateError.toString() }"></c:out><br/>
                    </label>
                </label>

                <input type="Submit" name="submit" value='Meet!' />
            </form:form>
    </body>
</html>
