<%--
    Document   : view-access
    Created on : 10/11/2013, 8:26:34 PM
    Author     : tristan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Access Log</title>
        <style>
            .display{
                border-collapse: collapse;
                border: 1px solid black;
                border-width: 3px;
                word-wrap: break-word;

            }
            .display td{
                border: 1px solid black;
                padding-left: 7px;
                padding-right: 7px;
                max-width: 300px;
            }
        </style>
    </head>
    <body>
        <h1>Access Log</h1>
        <table class="display">
            <thead>
            <th>
                Date Accessed
            </th>
            <th>
                User
            </th>
            <th>
                Thread Name
            </th>
        </thead>
        <c:forEach items="${ accessLog }" var="a">
            <tr>
                <td>
                    <c:out value="${ a.getDateAccessed() }"></c:out>
                </td>
                <td>
                    <c:out value="${ a.getUser().getFirstName() }"></c:out>
                    <c:out value="${ a.getUser().getLastName() }"></c:out>
                </td>
                <td>
                    <c:out value="${ a.getThreadName() }"></c:out>
                </td>
            </tr>
        </c:forEach>
    </table>
</body>
</html>


