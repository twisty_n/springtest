<?xml version="1.0" encoding="ISO-8859-1" ?>

<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
        <title>Login page</title>
        <style>
            .error {
                color: red;
            }
        </style>
    </head>
    <body>
        <h1>Welcome Citizen!!</h1>

        <p>
            <c:if test="${error == true}">
                <img src="${ pageContext.request.contextPath }/resources/nedry.gif"></img><br />
                <b class="error">Invalid login or password.</b>
            </c:if>
        </p>

        <form method="post" action="<c:url value='j_spring_security_check'/>" >
            <table>
                <tbody>
                    <tr>
                        <td>Login:</td>
                        <td><input type="text" name="j_username" id="j_username"size="30" maxlength="40"  /></td>
                    </tr>
                    <tr>
                        <td>Password:</td>
                        <td><input type="password" name="j_password" id="j_password" size="30" maxlength="32" /></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><input type="submit" value="Login" /></td>
                    </tr>
                </tbody>
            </table>
        </form>
        <c:if test="${error != true}">
            <br />
            <h3>From the pair that brought you "Oh god why's it still seg faulting?!"</h3>
            <h2>Andrew Dabson and Tristan Newmann present:</h2>
            <img src="${ pageContext.request.contextPath }/resources/assDue.jpg"></img><br />
            <h3>A story of terror and triumph against the evil forces of Java</h3>
            </c:if>
    </body>
</html>