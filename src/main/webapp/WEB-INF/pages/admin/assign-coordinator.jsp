<%--
    Document   : assign-coordinator
    Created on : 07/11/2013, 9:31:37 AM
    Author     : tristan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
                <p>
            <a href="${pageContext.request.contextPath}/admin/user-operation.html">Create or delete user!   </a>
            <a href="${pageContext.request.contextPath}/admin/group-operations.html">Create or Delete Research Group!   </a>
            <a href="${pageContext.request.contextPath}/admin/assign-coordinator.html">Assign New Coordinator!  </a>
            <a href="${pageContext.request.contextPath}/admin/view-all.html">View Everything!   </a>
            <a href="<c:url value="/j_spring_security_logout"></c:url>">Logout</a>
        </p>
        <h1>New Coordinator</h1>
        <form method="post" action="${pageContext.request.contextPath}/admin/doAssignNewCoordinator">
            <label>
                Select the research group
                <select name="group">
                    <c:forEach items="${groupList}" var="g" >
                        <option value="${ g.getGroupId() }">
                            <c:out value="${ g.getGroupName() }"></c:out>
                        </option>
                    </c:forEach>
                </select>
            </label>
            <br />
            <label>
                Select the new coordinator:
                <select name="newCoordinator">
                    <c:forEach  items="${ userList }" var="u">
                        <option  value="${ u.getUserId() }">
                            <c:out value="${ u.getFirstName() } ${ u.getLastName() }"></c:out>
                        </option>
                    </c:forEach>
                </select>
            </label><br />
            <input type="Submit" name="submit" value='Assign!' />
        </form>
    </p>
</body>
</html>
