<%--
    Document   : admin-landing
    Created on : 05/11/2013, 5:40:16 PM
    Author     : tristan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Admin Area</title>
    </head>
    <body>
        <h1>You have reached the admin landing page</h1>
        <p>
            Available Operations:
        </p>
        <p>
            <a href="${pageContext.request.contextPath}/admin/user-operation.html">Create or delete user!</a> <br />
            <a href="${pageContext.request.contextPath}/admin/group-operations.html">Create or Delete Research Group!</a> <br />
            <a href="${pageContext.request.contextPath}/admin/assign-coordinator.html">Assign New Coordinator!</a> <br />
            <a href="${pageContext.request.contextPath}/admin/view-all.html">View Everything!</a> <br />
        </p>
        <a href="<c:url value="/j_spring_security_logout"></c:url>">Logout</a>
    </body>
</html>
