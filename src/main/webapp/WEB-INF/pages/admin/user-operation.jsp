<%--
    Document   : add-user
    Created on : 06/11/2013, 11:35:52 AM
    Author     : tristan
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Add a user</title>
    </head>
    <body>
                <p>
            <a href="${pageContext.request.contextPath}/admin/user-operation.html">Create or delete user!   </a>
            <a href="${pageContext.request.contextPath}/admin/group-operations.html">Create or Delete Research Group!   </a>
            <a href="${pageContext.request.contextPath}/admin/assign-coordinator.html">Assign New Coordinator!  </a>
            <a href="${pageContext.request.contextPath}/admin/view-all.html">View Everything!   </a>
            <a href="<c:url value="/j_spring_security_logout"></c:url>">Logout</a>
        </p>
        <h1>Add a user</h1>

        <form:form method="post" commandName="user" action="${pageContext.request.contextPath}/admin/doCreateUser">
            <label>First Name:
                <form:input required="true" type="text" path="firstName" name="firstName" maxlength="30"/>
                <br />
                <form:errors style="color: red" path="firstName"></form:errors>
            </label><br />

            <label>Last Name:
                <form:input required="true" type="text" path="lastName" name="lastName" maxlength="30"/>
                <br />
                <form:errors style="color: red" path="lastName"></form:errors>
            </label><br />

            <label>e-Mail:
                <form:input type="email" path="emailAddress" name="emailAddress" maxlength="60"/>
                <br />
                <form:errors style="color: red" path="emailAddress"></form:errors>
            </label><br />

            <label>Username:
                <form:input required="true" type="text" path="userName" name="userName" maxlength="30"/>
                <em style="color: red">
                    <c:out default="" value="${ unameDuplicate.toString() }"></c:out>
                </em>
                <br />
                <form:errors style="color: red" path="userName"></form:errors>
            </label><br />

            <label>Password:
                <input required type="text" name="password" maxlength="30"/>
            </label><br />

            <label>
                Select Role:
                <select name="role">
                    <option value="1">Visitor</option>
                    <option value="3">Staff</option>
                    <option value="5">Student</option>
                </select>
            </label>

            <label><input type="Submit" name="submit" value='Create!' />
            </label><br />

                <c:if test="${ error != null }">
                    <p  style="color: red"><c:out value="${error}"></c:out></p>
                </c:if>


        </form:form>
                   <p>
                       Delete a user
                   </p>
                   <c:forEach items="${userList}" var="u" >
                       <p>
                           <a href="${pageContext.request.contextPath}/admin/doDeleteUser?userName=${ u.getUserId() }"  >
                               ${ u.getFirstName() } ${ u.getLastName() }
                            </a>
                       </p>
                   </c:forEach>
    </body>
</html>
