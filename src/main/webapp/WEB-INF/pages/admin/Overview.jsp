<%--
    Document   : Overview
    Created on : 05/11/2013, 10:49:23 PM
    Author     : tristan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Overview</title>
    </head>
    <body>
        <p>
            <a href="${pageContext.request.contextPath}/admin/user-operation.html">Create or delete user!   </a>
            <a href="${pageContext.request.contextPath}/admin/group-operations.html">Create or Delete Research Group!   </a>
            <a href="${pageContext.request.contextPath}/admin/assign-coordinator.html">Assign New Coordinator!  </a>
            <a href="${pageContext.request.contextPath}/admin/view-all.html">View Everything!   </a>
            <a href="<c:url value="/j_spring_security_logout"></c:url>">Logout</a>
        </p>
        <h1>This is everything in the database</h1>
        <c:forEach items="${userList}" var="u" >
            <p>
                ${u}
            </p>
        </c:forEach>
            <c:forEach items="${researchGroupList}" var="r" >
            <p>
                ${r}
            </p>
        </c:forEach>
    </body>
</html>


