<%--
    Document   : group-operations
    Created on : 06/11/2013, 4:59:29 PM
    Author     : tristan
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>GroupOps</title>
    </head>
    <body>
                <p>
            <a href="${pageContext.request.contextPath}/admin/user-operation.html">Create or delete user!   </a>
            <a href="${pageContext.request.contextPath}/admin/group-operations.html">Create or Delete Research Group!   </a>
            <a href="${pageContext.request.contextPath}/admin/assign-coordinator.html">Assign New Coordinator!  </a>
            <a href="${pageContext.request.contextPath}/admin/view-all.html">View Everything!   </a>
            <a href="<c:url value="/j_spring_security_logout"></c:url>">Logout</a>
        </p>
        <h1>Add a Group</h1>

        <form:form method="post" commandName="group" action="${pageContext.request.contextPath}/admin/doCreateRG">

            <label>
                Group Name:
                <form:input type="text" path="groupName" maxlength="30"/>
                <br />
                <form:errors path="groupName"></form:errors>
            </label>

            <label>
                Group Coordinator:
            <form:select path="groupCoordinator">
                <c:forEach  items="${ userList }" var="u">
                    <form:option value="${ u.getUserId() }">
                        <c:out value="${ u.getFirstName() } ${ u.getLastName() }"></c:out>
                    </form:option>
                </c:forEach>
            </form:select>
            </label>

            <label><input type="Submit" name="submit" value='Create!' />
            </label><br />

        </form:form >
                   <p>
                       Delete a Group
                   </p>
                   <c:forEach items="${groupList}" var="g" >
                       <p>
                           <a href="${pageContext.request.contextPath}/admin/doDeleteRG?groupId=${ g.getGroupId() }"  >
                               <c:out value="${ g.getGroupName() }"></c:out>
                            </a>
                       </p>
                   </c:forEach>
    </body>
</html>
